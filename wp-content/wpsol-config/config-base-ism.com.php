<?php 
defined( 'ABSPATH' ) || exit;
return array (
  'speed_optimization' => 
  array (
    'act_cache' => 0,
    'clean_cache' => 60,
    'act_compression' => 1,
    'add_expires' => 1,
  ),
  'disable_page' => 
  array (
    0 => '',
  ),
  'homepage' => 'http://base-ism.com',
  'disable_per_adminuser' => 1,
); 
