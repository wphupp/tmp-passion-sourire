<div class = "columns small-12 medium-6 single_blogue_left">
	<div class="blog_left">
		<img src = "<?php the_field('single_post_image'); ?> " /> 
	</div>
</div>
<div class = "columns small-12 medium-6 single_blogue_right">
  <div class="backbutton">
      <a class="button" href="<?php echo home_url(); ?>/blogue">
      <i class="icon-angle-left"></i> RETOUR AUX <b>BLOGUE</b>
    </a>
  </div>
	<div class="blog_right">
		<h1><?php the_title();?></h1>
			<?php the_content(); ?>
	</div>
  <div class="morecontent">
    <div class="morelinks">
      <?php the_field('read_more_content') ?>
    </div>
  </div>
</div>
