<?php get_header(); ?>
<?php
$code = get_sub_field('video_clip');
?>
<?php  ?>

 
<section>
	<article class = "single-sericesbg" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="container single-serviceouter">
			<div class="row">
				<div class="columns small-12 medium-6 large-6 single-serviceinner">
					<div class = "single-services-background overlay" style = "background-image: url(<?php the_field('services_image', 'option');   ?>);">
			 		</div>
					<div class="single_services_video">
			             

				          


								<?php if( have_rows('code_clip') ): ?>
									
									<?php while( have_rows('code_clip') ): the_row(); 

										// vars
										$code = get_sub_field('video_clip');
							                if( get_sub_field('video_clip') ) {

										?>
										
												<script type="text/javascript">
											        document.write(insertClipVzaar("<?php  echo $code; ?>", rootFr, "520", "275", "false", "s", ""));
											    </script>
											
											<?php
							                }
							                elseif( get_field('generic_image') )
							                {   
							           	 	?>

							           	 	<img src="<?php the_field('generic_image'); ?>">

							           	 	<?php
							                }
							           	 	else
							                {
							            	?>
							                	<img src="<?php the_field('services_image', 'option');   ?>" width="615px"  /> 
							            <?php
							                }

											?>
									<?php endwhile; ?>
								<?php endif; ?>
			        </div>	

				</div>
				<div class="services_single_outer columns small-12 medium-6 large-6">
					<div class="services_single">
						<div class="backbutton">
							<a class="button" href="<?php echo home_url(); ?>/services">
							<i class="icon-angle-left"></i> RETOUR AUX <b>SERVICES</b></a>
						</div>
						<h1>
							<?php the_title();?>
						</h1>
						<div class="single_services_description">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</article>
</section>

<?php get_footer(); ?>