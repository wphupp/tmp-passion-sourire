 <div class="bottom-header">
 	<div class="header_top_outer">
	 	<div class = "site_logo">
	 		<a href = "<?php echo home_url(); ?>">
		 		<?php if ( get_field( 'logo', 'option' ) )
				 { 
					$aicon = get_field( 'logo', 'option' );
					$icon = file_get_contents( $aicon);
					echo $icon;
				 } 
				?>
				<?php /* if ( get_field( 'logo','option' ) ){
	                    $icon = get_field( 'logo','option' );
	                    $icon = str_replace( site_url(), '', $icon);
	                    include(ABSPATH . '$icon'); 
                    } */
                    
                ?>
			</a>
	 	</div>
	 	<div class = "menu_title">
	 		<div class = "today_time">
	 			<h1><?php echo do_shortcode('[open_today]'); ?></h1>
	 		</div>
	 		<button class="hamburger hamburger-5" id="showLeft"><h1>MENU</h1><h1 class = "close_title" >FERMER</h1><span></span></button>
			  		<?php global $config; ?>
			 <button>
			   <h1 class="language"><?php if($config['wpml']){languages_switch_header();} ?></h1>
			</button>
	 	</div>
	 </div>
 	<!-- class for menu : showLeft showRight showTop showBottom showLeftPush showRightPush -->
 </div>

