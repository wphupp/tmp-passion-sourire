
<?php get_header(); 

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>
<section>
</section>
<div class="front-background-title">
	<div class = "front-background" style="background-image: url('<?php the_field('front_background_image'); ?>')">
	</div>
	<h1>
		<?php the_field('header_title'); ?>
	</h1>
</div>
<section>
	<div class="row">
		<div class="container main-bg-content">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class = "row_of_images">
				<?php the_content(); ?>
				</div>
			</article>
		</div>
	</div>
</section>
<!-- if carousel is activate -->
<div class = "caroussel_block">
	<?php if($config['caroussel'] ): ?>
		<div id="carousel" class="owl-carousel"  style="background-image: url('<?php the_field('background_silder', 'option');   ?>')"; >
			<?php if( have_rows('slides') ): ?>
				<?php while ( have_rows('slides') ) : ?> 
					<?php the_row(); ?>
					<div class="item">
						<div class="centered">
							<h2><?php the_sub_field('titre'); ?></h2>
							<?php if(get_sub_field('sous_titre') != ""): ?>
								<h2><?php the_sub_field('sous_titre'); ?></h2>
							<?php endif; ?>
							<?php if(get_sub_field('description') != ""): ?>
								<p><?php the_sub_field('description'); ?></p>
							<?php endif; ?>
							<?php if( have_rows('button') ): ?>
								<?php while ( have_rows('button') ) : ?> 
									<?php the_row(); ?>
									<a <?php echo (get_sub_field('lien_externe'))?'target="_blank"':''; ?>href="<?php the_sub_field('url'); ?>"><?php the_sub_field('texte'); ?></a>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	<?php endif;  ?>
</div>

<div class="light_main">
	<div class='lightbox1'>
	  <div class='lightbox-inner'>
	    <div class='content'>
		    <div class = "close">
		    	<i class="fa fa-times" aria-hidden="true"></i>
		    </div>
			<div class = "box_content">
	    		<?php the_field('banner_modal'); ?>
	    	</div>
	    </div>
	  </div>
	</div>
</div>



<?php get_footer(); ?>