<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>" autocomplete="off">
	<h3><?php echo __('Trouvez ce que vous cherchez !', 'dialogue'); ?></h3>
  <span class="search" aria-hidden="true"></span><input name="s" type="text" value="" placeholder="Rechercher ..." />
</form>