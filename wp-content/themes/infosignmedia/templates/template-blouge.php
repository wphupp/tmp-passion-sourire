<?php 
/**
 * Template Name: Blouge
*/
get_header(); ?>
<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<?php if($thumb['0']) { ?>
<section class="full-cover" style="<?php backgroundImage($thumb['0']); ?>">
</section>
<?php } ?>
<section>
	<div class="row">
		<div class="blouge_outer">
			<article id="post-<?php the_ID(); ?>">
				<?php the_content(); ?>
			</article>
		</div>
	</div>
</section>

<?php get_footer();?>