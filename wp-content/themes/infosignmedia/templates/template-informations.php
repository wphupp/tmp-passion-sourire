<?php
/**
 * Template Name: Informations
*/
get_header();
?>
  <?php if($thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )) :?>
  <?php endif; ?>
  <?php the_content(); ?>
 
  <section class="information">
    <div class="row">
      <div class="container info-outer">
        <?php
        while ( have_posts() ) : the_post();?>
        <div class="row">
          <div class="columns small-12 medium-12 large-12">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="infoarmationp-section columns small-12 medium-6 large-6 info-left">
                    <div class = "left-image">
                      <img class = "info-left-image" src="<?php the_field('single_image_information_page', 'option'); ?>">
                    </div>
                        <div class="information-widget-section blue-heading columns small-12 medium-6 large-6 info-left-heading"> 
                          <div class = "info-left-heading1">
                            <?php if(is_active_sidebar('informationbox1')){ dynamic_sidebar('informationbox1'); } ?>
                          </div>
                        </div>  
                        
                        <div class="information-widget-section blue-heading columns small-12 medium-6 large-6 info-right-heading">  
                          <div class = "info-right-heading1">  
                              <?php if(is_active_sidebar('informationbox2')){ dynamic_sidebar('informationbox2'); } ?>
                          </div>
                        </div>
                        
                        <div class="information-widget-section blue-heading columns small-12 medium-6 large-6 info-left-heading2"> 
                          <div class = "info-left-heading3">            
                            <?php if(is_active_sidebar('informationbox3')){ dynamic_sidebar('informationbox3'); } ?>
                          </div>
                        </div>
                        
                        <div class="information-widget-section dark-blue columns small-12 medium-6 large-6 info-right-heading2 ">  
                          <div class = "info-right-heading3">    
                            <?php if(is_active_sidebar('informationbox4')){ dynamic_sidebar('informationbox4'); } ?>
                          </div>
                        </div>  
                </div>
                    <div class="infoarmationp-section left-section columns small-12 medium-6 large-6 info-right">
                        <div class="info-right-title">
                          <h1><?php the_field('information_title'); ?></h1>
                        </div>
                        <div class="info_description ">
                           <?php the_field('information_content'); ?>
                        </div>
                    </div>
                </article>
              </div>
          </div>
        <?php endwhile;?>
      </div>
    </div>
</section>






<?php get_footer(); ?>