<?php
/**
 * Template Name: Services
 *
*/
get_header();
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>
<section class="services">
  <div class="row">
    <div class="container service-outer">
        <div class="columns small-12 medium-12 large-12 service-inner">
            <div class="service-image infoarmationp-section columns small-12 medium-6 large-6" style="background-image: url(<?php the_field('services_image'); ?>);">
            </div>
                <div class="infoarmationp-section columns small-12 medium-6 large-6 service-data-right">
                    <div class="container">
                         <div class="page_title">                 
                            <h1>
                                <?php the_field('services_title'); ?>
                            </h1>             
                        </div>
                    </div> 
                    <div class="services_page_description">
                         <?php
                            if($config['services_category']){
                                // Classé par catégories
                                $args = array( 
                                    'post_type' => 'services',
                                    'posts_per_page' => -1, 
                                    'post_parent' => 0, 
                                    'orderby'  => 'menu_order',
                                    'order' => 'ASC'
                                );
                                $services = new WP_Query( $args );
                            ?>
                        <div class="services_list">
                            <div class="row">
                                <article id="post-<?php echo $post->ID; ?>" <?php post_class(); ?>>
                                    <ul class="row">
                                        <?php
                                        while ( $services->have_posts() ) : $services->the_post(); ?>                        
                                                <?php
                                                    $args = array( 
                                                        'post_type' => 'services',
                                                        'posts_per_page' => -1,
                                                        'post_parent' => $post->ID, 
                                                        'orderby'  => 'title',
                                                        'order' => 'ASC'
                                                    );
                                                    $service_children = get_posts( $args ); ?>
                                                    <div class="columns small-12 medium-6 my_service_list">
                                                        <li>
                                                            <a href="<?php the_permalink($post->ID); ?>">
                                                                <p><?php echo get_the_title(); ?></p>
                                                            </a>
                                                        </li>
                                                    </div>
                                                 <?php endwhile; 
                                                } else {                
                                                      ?>
                                                <?php } ?>
                                            <?php wp_reset_query(); ?>
                                        </ul>   
                                </article>
                            </div>
                        </div>
                        <div class="page_description">
                            <?php the_field('services_description'); ?>
                        </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>