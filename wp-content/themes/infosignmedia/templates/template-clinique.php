<?php
/**
 * Template Name: Clinique
*/
get_header(); ?>
<?php if($thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )) :?>
  <section class="full-cover" style="<?php backgroundImage($thumb['0']); ?>"></section>
<?php endif; ?>
<section class="content">
  <div class="row">
  	<div class="clinique-container clinical-content">
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php the_content(); ?>
      </article>
    </div>
  </div>
</section>
<section>
 <div class="row box">
    <?php if( have_rows('photo_clinique') ):
     while ( have_rows('photo_clinique') ) : the_row();?>
     <a href="<?php the_sub_field('photo'); ?>" data-lightbox="roadtrip">
       <div class="columns small-12 medium-4 bgImage" style="<?php backgroundImage(get_sub_field('photo')); ?>"></div>
     </a>
    <?php endwhile;endif; ?>
  </div>
</section>

<!-- <div class="virtual_visit_outer">
  <div class="virtual_visit_lightbox">
    <div class="virtual_visit_lightbox_inner">
      <div class="content">
        <div class = "close_visit">
          <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div class = "virtual_visit_content">
          <iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m0!4v1504519981422!6m8!1m7!1s2gKIxBFqF2Bzphih-50yLQ!2m2!1d45.40878286!2d-74.03785393!3f298.3300936609798!4f-2.620682683363171!5f0.7820865974627469" width="600" height="450" frameborder="0" allowfullscreen="allowfullscreen">
          </iframe>
        </div>
      </div>
    </div>
  </div>
</div> -->
<?php get_footer();?>
