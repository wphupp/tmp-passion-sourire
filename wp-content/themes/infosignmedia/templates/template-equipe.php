

<?php
/**
* Template Name: Equipe
*/
get_header();
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$args = array( 'post_type' => 'employes', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order');
$equipes = new WP_Query( $args );
?>
<section class="content">
    <div class="row">
        <div class="equipe-container">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php the_content(); ?>
            </article>
        </div>
    </div>
</section>
<?php get_footer();?>