<?php global $config; ?>
</section>
<footer>
  <!--widgets footer-->
  <div class="widgets footer_outer">
    <div class="row footer_row">
      <div class = "columns small-12 medium-6 footer_inner">
        <div class="columns small-12 medium-6 footer_address">
          <?php if(is_active_sidebar('footer1')){ dynamic_sidebar('footer1'); } ?>
        </div>
        <div class="columns small-12 medium-6 footer_contact">
          <?php if(is_active_sidebar('footer2')){ dynamic_sidebar('footer2'); } ?>
        </div>
      </div>
      <div class="columns small-12 medium-6 open_hour_footer">
          <?php if(is_active_sidebar('footer3')){ dynamic_sidebar('footer3'); } ?>
      </div>
    </div>
  </div>
  <section class="copyright">
    <?php the_field('copyright_text', 'option'); ?>
  </section>
</footer>
  <!-- copyright -->

  
</div> <!--site_content-->
</div><!-- end wrap -->

<?php if($config['back_to_top_button']) : ?>
  <div id="back_top">
    <span class="ti-angle-up"></span>
  </div>
<?php endif; ?>

<?php if($config['responsive_debug']) : ?>
<div class="debug_responsive">
  <span></span>
</div>
<?php endif; ?>

<?php wp_footer(); ?>

<!-- pour la map dynamique avec page option -->
<?php $map = array();
if( have_rows('informations_de_cliniques', 'option') ) {
  while( have_rows('informations_de_cliniques', 'option') ){
    the_row();
    $map[] = array(
      get_sub_field('clinic_name'),
      'undefined',
      get_sub_field('clinic_phone'),
      get_sub_field('clinic_email'),
      'undefined',
      get_sub_field('latitude'),
      get_sub_field('longitude'),
      get_assets().'images/marker4.png'
      );
  }
} ?>
<!-- <script type="text/javascript">
    
    var map;
      function initMap() {

        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
          info: {
            icon: iconBase + 'info-i_maps.png'
          }
        };
        // 45.4080195,-74.0363718
        // 45.40802,-74.036372
        var myLatLng = {lat: 45.408793, lng: -74.037490};
        // 74.037486 45.408793
        map = new google.maps.Map(document.getElementById('mymap'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
        });
      }
</script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnvYkmd13FaMUzeSoqM513uH1pGgqNuoc&libraries=places&callback=initMap" async defer></script> -->
<!-- Javascript utility helper variable -->
<script type="text/javascript">
  var mapArray = <?php echo json_encode($map); ?>;
  var mapStyle = <?php echo json_encode(strip_tags(get_field('style_map', 'option'))); ?>;
</script>



</body>
</html>
