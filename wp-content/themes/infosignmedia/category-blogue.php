<?php get_header(); ?>

<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id('29'), 'full' );?>
<section class="full-cover" style="<?php backgroundImage($thumb['0']); ?>"></section>
<section>
  <div class="row">
    <div class="container">
      <div class="row">
      <div class="columns small-12 medium-9">
          <?php if ( have_posts() ) : ?>
            <?php
            the_archive_title( '<h1>', '</h1>' );
            the_archive_description( '<div class="taxonomy-description">', '</div>' );
            endif;
            ?>
            <?php $query = new WP_Query( 'cat=-4,-3, -5' ); ?>

            <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();    ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                <?php the_excerpt();?>
              </article><!-- #post-## -->

<?php // End the loop.
endwhile; ?>
</div>
<aside class="columns small-12 medium-3">
  <?php if(is_active_sidebar('sidebar1')){ dynamic_sidebar('sidebar1'); } ?>
  <?php if(is_active_sidebar('sidebar2')){ dynamic_sidebar('sidebar2'); } ?>
</aside>
</div>

<?php

else :
  get_template_part( 'content', 'none' );

endif;
?>
</div>
</div>
</section>
<?php get_footer(); ?>