<?php
/**
* The template for displaying all single posts and attachments
*
* @package WordPress
*/

get_header(); ?>

<section>
	<div class="row">
		<div class="container single-info-outer">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="infoarmationp-section left-section columns small-12 medium-12 large-12 
			single-info-inner">
					<div class="row">
						
					</div>
					 <div class="infoarmationp-section columns small-12 medium-6 large-6 single-info-left"> 
							<?php if( get_field('right_image') ): ?>
							<div class = "singleinfo-left-image">		
								<img class = "single-info-left-image1" src="<?php the_field('right_image'); ?>" />
							</div>
							<?php  else: ?>
		                	<div class = "single-left-image">	
								<img class = "single-info-left-image" src="<?php the_field('single_image_information_page', 'option'); ?>">
		                	</div>
							<?php endif; ?>
								 
							<div class="information-widget-section blue-heading columns small-12 medium-6 large-6 info-left-heading"> 
                          		<div class = "info-left-heading1"> 
                          			<?php if(is_active_sidebar('informationbox1')){ dynamic_sidebar('informationbox1'); } ?>
                          		</div>
                        	</div>  
                        
	                        <div class="information-widget-section blue-heading columns small-12 medium-6 large-6 info-right-heading">  
                          		<div class = "info-right-heading1">   
	                         		 <?php if(is_active_sidebar('informationbox2')){ dynamic_sidebar('informationbox2'); } ?>
	                         	</div>
	                        </div>
                        
                        	<div class="information-widget-section blue-heading columns small-12 medium-6 large-6 info-left-heading2"> 
                          		<div class = "info-left-heading3"> 
                          			<?php if(is_active_sidebar('informationbox3')){ dynamic_sidebar('informationbox3'); } ?>
                          		</div>
                        	</div>
                        
                        	<div class="information-widget-section dark-blue columns small-12 medium-6 large-6  info-right-heading2 ">  
                         		 <div class = "info-right-heading3">     
                          			<?php if(is_active_sidebar('informationbox4')){ dynamic_sidebar('informationbox4'); } ?>
                          		</div>
                        	</div>
						</div>
					<div class="infoarmationp-section left-section columns small-12 medium-6 large-6 single-info-right">
	                      	<?php
								while ( have_posts() ) : the_post();
								?>
								<div class="row">
								  <div class="columns small-12 medium-9 single_information">
								    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								      <h1><?php the_title();?></h1>
								      	<div class = "single_information_description">
									      	<div class="single_information_video">
									            <?php 
									            $code = get_field('video_clip');
									                if( get_field('video_clip') ) {
									                	?>
									                    <script type="text/javascript">
												            document.write(insertClipVzaar("<?php  echo $code; ?>", rootFr, "520", "275", "false", "s", ""));
												        </script>
												        <?php
									                }
									            ?>
					       					</div>	
								      		<?php the_content(); ?>
								      	</div>
								    </article>
								  </div>
								  <aside class="columns small-12 medium-3 sidebar">
								    <?php if(is_active_sidebar('sidebar1')){ dynamic_sidebar('sidebar1'); } ?>  
								  </aside>
								</div>
								<?php	
								endwhile;
							?>
                	</div>
				</div>
				
			</article>
		</div>
	</div>
</section>

<?php get_footer(); ?>