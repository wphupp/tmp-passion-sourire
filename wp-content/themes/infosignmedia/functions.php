<?php
require_once(__dir__.'/config.php');

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);

/* ================ JAVASCRIPT ============== */
//function for javascript
function ism_js(){
	$assets_dir = get_assets();
	wp_enqueue_script( 'ism-jquery', $assets_dir . 'js/vendor/jquery-1.12.3.min.js');
	wp_enqueue_script( 'ism-foundation', $assets_dir . 'js/vendor/foundation.min.js');
	wp_enqueue_script( 'ism-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBnvYkmd13FaMUzeSoqM513uH1pGgqNuoc&extension=.js');
	wp_enqueue_script( 'ism-tweenMax', $assets_dir . 'js/vendor/tweenMax.min.js');
	wp_enqueue_script( 'ism-ease-in', $assets_dir . 'js/vendor/easeIn.js');
	wp_enqueue_script( 'ism-detect', $assets_dir . 'js/vendor/detect.js');
	wp_enqueue_script( 'ism-classie', $assets_dir . 'js/vendor/classie.js');
	wp_enqueue_script( 'ism-fastclick', 'https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js');
	wp_enqueue_script( 'ism-config', $assets_dir . 'js/config.min.js');
	wp_enqueue_script( 'ism-maps', $assets_dir . 'js/map.min.js');
	wp_enqueue_script( 'ism-lightbox', $assets_dir . 'js/vendor/lightbox.min.js');
	wp_enqueue_script( 'ism-script', $assets_dir . 'js/main.min.js');
	wp_enqueue_script( 'ism-menu', $assets_dir . 'js/menu.min.js');
	wp_enqueue_script( 'ism-owl-carousel', $assets_dir . 'js/vendor/owl.carousel.min.js');
	wp_enqueue_script( 'ism-carousel', $assets_dir . 'js/carousel.min.js');
	wp_enqueue_script( 'ism-mCustomScrollbar', $assets_dir . 'js/jquery.mCustomScrollbar.concat.min.js');
	wp_enqueue_script( 'ism-custom', $assets_dir . 'js/vendor/custom.js');
	wp_enqueue_script( 'ism-lity', $assets_dir . 'js/vendor/lity.js');
	wp_enqueue_script( 'ism-fancybox', $assets_dir . 'js/vendor/jquery.fancybox.js');
}

//add javascript
add_action( 'wp_enqueue_scripts', 'ism_js');

//Move the javascript in the footer
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_enqueue_scripts', 5);

//css
function add_stylesheet_to_head() {
	echo "<link rel='stylesheet' href='".get_assets()."/styles/css/styles.css'>";
}

add_action('wp_head', 'add_stylesheet_to_head', 10);


/* =============== REGISTER MENU =============== */
function infosign_primary() {
	$defaults = array(
		'theme_location'  => 'primary',
		'echo'            => true,
		'depth'           => 0,
		'menu_class' 	  => 'primary_menu',
		'container' 	  => 'nav'
		);

	wp_nav_menu( $defaults );

}
function infosign_secondary() {
	$defaults = array(
		'theme_location'  => 'top',
		'echo'            => true,
		'menu_id' 		  => 'top-menu',
		'depth'           => 0,
		'container_class' => 'col-md-6 col-sm-12 top-nav',
		'menu_class' 	  => 'inline-block list-unstyled',
		'container' 	  => 'nav'
		);

// wp_nav_menu( $defaults );
}
function infosign_sidebar() {
	$defaults = array(
		'theme_location'  => 'sidebar',
		'echo'            => true,
		'menu_id' 		  => 'top-menu',
		'depth'           => 0,
		'container_class' => 'col-md-6 col-sm-12 top-nav',
		'menu_class' 	  => 'inline-block list-unstyled',
		'container' 	  => 'nav'
		);

	wp_nav_menu( $defaults );
}


register_nav_menu( 'primary', __( 'Menu principal', 'infosign' ) );
register_nav_menu( 'secondary', __( 'Menu secondaire', 'infosign' ) );
register_nav_menu( 'sidebar', __( 'Menu sidebar', 'infosign' ) );
/*=========== Thumbnail ===============*/
add_theme_support( 'post-thumbnails' );

/*============= ACTIVE SIDEBARS ==================*/

function infosign_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => 'Pages Informations',
		'description' => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => 'Pages Blogue',
		'description' => 'Used on every page BUT the homepage page template.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3',
		));

	register_sidebar(array(
		'id' => 'footer1',
		'name' => 'Footer 1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));

	register_sidebar(array(
		'id' => 'footer2',
		'name' => 'Footer 2',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));

	register_sidebar(array(
		'id' => 'footer3',
		'name' => 'Footer 3',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));
	register_sidebar(array(
		'id' => 'informationbox1',
		'name' => 'Informations 1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));
	register_sidebar(array(
		'id' => 'informationbox2',
		'name' => 'Informations 2',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));
	register_sidebar(array(
		'id' => 'informationbox3',
		'name' => 'Informations 3',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));
	register_sidebar(array(
		'id' => 'informationbox4',
		'name' => 'Informations 4',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
		));
}
add_action( 'widgets_init', 'infosign_register_sidebars' );

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

/*============ COMMENTAIRES ===============*/

// Disable support for comments and trackbacks in post types
function ism_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'ism_disable_comments_post_types_support');

// Close comments on the front-end
function ism_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'ism_disable_comments_status', 20, 2);
add_filter('pings_open', 'ism_disable_comments_status', 20, 2);

// Hide existing comments
function ism_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'ism_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function ism_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'ism_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function ism_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'ism_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function ism_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'ism_disable_comments_dashboard');

// Remove comments links from admin bar
function ism_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'ism_disable_comments_admin_bar');

/*============= EXTRAIT ================= */
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}

/* ============= HELPER FUNCTION ============ */

/* Function to get assets path*/
function get_assets(){
	return get_stylesheet_directory_uri().'/assets/';
}

/* Function to know if we are on mobile in php */
function isMobile() {
	return (preg_match('/android|blackberry|htc|iphone|ipad|ipaq|ipod|mobile/', strtolower($_SERVER['HTTP_USER_AGENT'])))?true:false;
}

function backgroundImage($image){
	echo "background: url('".$image."') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;";
}

function is_page_child($pid) {// $pid = The ID of the page we're looking for pages underneath
global $post;         // load details about this page
$anc = get_post_ancestors( $post->ID );
foreach($anc as $ancestor) {
	if(is_page() && $ancestor == $pid) {
		return true;
	}
}
if(is_page()&&(is_page($pid)))
return true;   // we're at the page or at a sub page
else
return false;  // we're elsewhere
};

//afficher la cat/gorie au lieu d'archive
add_filter( 'get_the_archive_title', function ($title) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>' ;
	}
	return $title;
});

/*========== WORDPRESS CHANGE DEFAULT =============== */
// Changer le footer dans l'admin
function remove_footer_admin () {
	echo 'Design et développement par <a href="http://infosignmedia.com" target="_blank">Infosignmedia</a></p>';
}
add_filter('admin_footer_text', 'remove_footer_admin');


/* Ajouter la page options de ACF pro */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

/* Changement de langue, montrant juste la langue non-utilisee */

function languages_switch_header() {
	$languages = icl_get_languages('skip_missing=0&orderby=code');
	if (!empty($languages)) {
		foreach ($languages as $l) {
			if (!$l['active']) {
				echo '<div class="langues"><a href="' . $l['url'] . '">';
				echo $l['native_name'];
				echo '</a></div>';
			}
		}
	}
}
add_shortcode('language-switcher', 'languages_switch_header');


function my_logincustomCSSfile() {
	wp_enqueue_style('login-styles', get_template_directory_uri() . '/assets/styles/css/login.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

//function for exerpt
function wpdocs_excerpt_more( $more ) {
	return '<a class="readmore_exerpt" href="'.get_the_permalink().'" rel="nofollow"> [...]</a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );



//=======  Allow SVG =======//
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

  global $wp_version;
  if ( $wp_version !== '4.7.1' ) {
     return $data;
  }

  $filetype = wp_check_filetype( $filename, $mimes );

  return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
  ];

}, 10, 4 );

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

function fix_svg() {
  echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'fix_svg' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> 'page=acf-options-header',
	));
	
	 
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
		
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
		
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Informations Image',
		'menu_title'	=> 'Information Image',
		'parent_slug'	=> 'theme-general-settings',
		
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Services Image',
		'menu_title'	=> 'Services Image',
		'parent_slug'	=> 'theme-general-settings',
		
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'home background image',
		'menu_title'	=> 'Home Background Image',
		'parent_slug'	=> 'theme-general-settings',
		
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'File Not Found',
		'menu_title'	=> 'file not found',
		'parent_slug'	=> 'theme-general-settings',
		
	));
	
}



