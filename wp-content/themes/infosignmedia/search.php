<?php
/**
* The template for displaying search results pages.
*
* @package WordPress
*/

get_header(); ?>
<section>
	<div class="row search_result">
		<div class="container">
			<?php if ( have_posts() ) : ?>
				<h1><?php printf( __( 'Résultats de recherche pour: %s', 'InfoSignMedia' ), get_search_query() ); ?></h1>
				<?php
				while ( have_posts() ) : the_post(); ?>
				<?php
				get_template_part( 'content', 'search' );
				endwhile;
				the_posts_pagination( array() );
				else :
					get_template_part( 'content', 'none' ); ?>
			<?php	endif;
			?>
		</div>
	</div>
</section>
<?php get_footer(); ?>