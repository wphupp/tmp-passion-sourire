<?php
/**
* The main template file
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link http://codex.wordpress.org/Template_Hierarchy
*/

get_header(); ?>
<section>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="columns  small-12 medium-9">
          <?php if ( have_posts() ) : ?>

            <?php if ( is_home() && ! is_front_page() ) : ?>
              <h1><?php single_post_title(); ?></h1>

            <?php endif; ?>

            <?php
            while ( have_posts() ) : the_post();
            get_template_part( 'content', get_post_format() );
            endwhile;

            the_posts_pagination( array() );

            else :
              get_template_part( 'content', 'none' );

            endif;
            ?>
          </div>
          <aside class="columns  small-12 medium-3">
            <?php get_sidebar(); ?>
          </aside>
        </div>
      </div>
    </div>
  </section>
  <?php get_footer(); ?>
