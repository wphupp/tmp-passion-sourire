<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>
<section class="error-404 not-found">
	<div class="row">
		<div class="container">
			<div class="centered_404">
				<img src = "<?php the_field('404_image', 'option');?>" />
				<h1>File Not Found</h1>
				<p>Sorry, that file doesn’t live here anymore. It might have been moved or made private.</p>
				<a href = "<?php echo home_url(); ?>">Back To Home</a>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>