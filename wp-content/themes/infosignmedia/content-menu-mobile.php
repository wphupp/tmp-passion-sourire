<!--nav-mobile-->
<nav class="menu-mobile menu-mobile-vertical menu-mobile-left menu_hamburger_scroll" id="menu-mobile-s1">
	<div class = "menu_inner">
		<?php infosign_primary(); ?>
		<div class="contact_detail">
			<div class="columns small-7">
				<div class="add">
					<?php the_field('menu_address', 'option'); ?>
				</div>
			</div>
			<div class="columns small-5">
				<div class="menu_contact_detail">
					<div class="menu_tel">
						 <a href="tel:<?php the_field('phone_number_href', 'option'); ?>"><strong><?php the_field('phone_number', 'option'); ?></strong> </a>
					</div>
					<div class="menu_fax">
						 <a href="fax:<?php the_field('fax_number_href', 'option'); ?>"><?php the_field('fax_number', 'option'); ?></a>
					</div>
					<div class="menu_email">
						<a href="mailto:<?php the_field('menu_email', 'option'); ?>"><strong><?php the_field('menu_email', 'option'); ?></strong></a>
					</div>
				</div> 
			</div>
		</div>	
		<div class="menu_map">
			<div class="map_img">
				<img src="<?php the_field('map_img', 'option'); ?>">
				<a href="<?php the_field('map_link', 'option'); ?>" target="_blank"></a>
			</div>
		</div>
	</div>
</nav>


 <!--  Posibilité menu mobile
  class="menu-mobile menu-mobile-vertical menu-mobile-left" id="menu-mobile-s1"
  class="menu-mobile menu-mobile-vertical menu-mobile-right" id="menu-mobile-s2"
  class="menu-mobile menu-mobile-horizontal menu-mobile-top" id="menu-mobile-s3"
  class="menu-mobile menu-mobile-horizontal menu-mobile-bottom" id="menu-mobile-s4" -->