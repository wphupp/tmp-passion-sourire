<?php
/**
* The template for displaying all single posts and attachments
*
* @package WordPress
*/

get_header(); ?>

<section>
	<div class="row">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="row">
				<?php
				while ( have_posts() ) : the_post();
				get_template_part( 'content', 'posts' );
				//the_post_navigation( array() );
				endwhile;
				?>
			</div>
		</article>
	</div>
</section>

<?php get_footer(); ?>