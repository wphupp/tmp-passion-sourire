// $(function(){
//   var boxRed = $('.open_effect');
//   var boxGreen = $('.menu-mobile-right');
//   $('.hamburger').click(function(){
//     boxRed.toggleClass('zoomIn zoomOut');
//     boxGreen.toggleClass('zoomIn zoomOut');
//   });
// });




$(document).ready(function(){
 
    // $.ajaxSetup({cache:false});
    // $(".accordiontitle .post-link").click(function(){
    //     var post_link = $(this).attr("href");
    //     $("#single-post-container").html("content loading");
    //     // $("#single-post-container").load(post_link + " .single_services_video" );
    //     $( "#single-post-container" ).load( post_link + "  .single_services_outer", function( response, status, xhr ) {
    //       if ( status == "error" ) {
    //         var msg = "Sorry but there was an error: ";
    //         $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
    //       }
    //     });
    // return false;
    // });
 

    $(".hamburger").click(function(){
        $("body").toggleClass("menu_open");
    });


if($(window).width() >= 768) {
    $('.single_mobile').remove();
} else {
    $('.single_mobile').add();
}

  if(window.matchMedia('(min-width: 768px)').matches) {
      (function($){
          $(window).on("load",function(){
              $(".first_description").mCustomScrollbar();
              $(".hometab_text").mCustomScrollbar();
              $(".left-scroll").mCustomScrollbar();
              $(".clinique-left-scroll").mCustomScrollbar(); 
              $(".services_page_description").mCustomScrollbar(); 
              $(".equip-heading").mCustomScrollbar(); 
          });
      })(jQuery);
  } 
  function mediaSize() 
  { 
        if (window.matchMedia('(max-width: 767px)').matches) {
             $( ".service_right" ).after( $( ".service_left" ) );
        } else {
             $( ".service_left" ).after( $( ".service_right" ) );
        }
        if (window.matchMedia('(max-width: 768px)').matches) {
          $(document).ready(function($) {
              // Equipe Page Accordion
              $('.equip-left-main').on('click', function() {
                  $parent_box = $(this).closest('.equipe_box');
                  $parent_box.siblings().find('.equipe_panel').hide();
                  $parent_box.find('.equipe_panel').toggle();
                  $grandfather = $parent_box.closest('.equipe_accordian');
                  $grandfather.siblings().find('.equipe_panel').hide();
                  $("body, html").animate({
                    scrollTop: $(this).position().top
                });
              });

              $('.equip-left-main').click(function(){
                  var self = $(this);
                  self.find('.equipe_panel').toggle();
                  self.toggleClass('active');
                  $('.equip-left-main').not(self).removeClass('active');
              });
          });
        }
    };
      mediaSize();
      window.addEventListener('resize', mediaSize, false);  
});
  $(document).ready(function() {
    $(document).foundation();
  });
  $(document).ready(function($) {
    $('.current_post').click(function(){
      var self = $(this);
      $('.current_post li').not(self).removeClass('post_active');
      if(!self.hasClass("post_active")){
          self.addClass('post_active');
      }
      else
      {
          self.removeClass('post_active');
      }
    });
  });

$(document).ready(function($) {
  // Service Page Accordion
  $('.accordiontitle').click(function(){
      var self = $(this);
      self.find('.servicepanel').toggle();
      $('.accordiontitle li').not(self).removeClass('active');
      $('.accordiontitle').not(self).removeClass('current');
      if(!self.hasClass("current")){
        self.addClass('current');
      }
      else
      {
        self.removeClass('current');
      }
  });
  // Information Accordion
  $('.myaccordion').click(function(){
      var self = $(this);
      self.find('.mypanel').toggle();
      $('.myaccordion').not(self).removeClass('selected');
      if(!self.hasClass("selected")){
        self.addClass('selected');
      }
      else
      {
        self.removeClass('selected');
      }
  });
});

$(document).ready(function() {
  // Service Page Accordion
  $('.ownaccordion').on('click', function() {
      $parent_box = $(this).closest('.service_box');
      $parent_box.siblings().find('.servicepanel').hide();
      $parent_box.find('.servicepanel').toggle();
      $("body, html").animate({
        scrollTop: $(this).position().top
    });
  });
  // Information Page Accordion
  $('.myaccordion').on('click', function() {
      $parent_box = $(this).closest('.mybox');
      $parent_box.siblings().find('.mypanel').hide();
      $parent_box.find('.mypanel').toggle();
      $grandfather = $parent_box.closest('.info_list_mobile');
      $grandfather.siblings().find('.mypanel').hide();
      $grandfather.siblings().find('.myaccordion').removeClass('selected');
      $("body, html").animate({
        scrollTop: $(this).position().top
    });
  });
});

$(document).ready(function() {
      var $window = $(window);
      function checkWidth() {
          var windowsize = $window.width();
          if (windowsize < 1199) {
              $( ".contact_section" ).after( $( ".contact_map" ) );
          }
      }
      checkWidth();
      $(window).resize(checkWidth);
  });

  $(document).ready(function(e) {
     $(".hamburger").on('click', function() {
     $(".menu-mobile-open").fadeIn();
    });
  });  


  function classExistAndRemove(className,parentClass){
    if ($(className).hasClass("over-menu")) {
        console.log('class exist should remove');
        $(className).toggleClass("over-menu");
    }
    if ($(parentClass).hasClass("active")) {
        $(parentClass).toggleClass("active");
    }
  }

  function activeClassAddRemove(current){
      if ( $(current).hasClass('active') ) {
          $(current).removeClass('active');
      } else {
          $(current).addClass('active');    
      }
  }

  $(window).load(function() {
      if ( $('body').hasClass('page-template-template-informations') ) {
          $(".menu-les-formulaires-container").toggleClass("over-menu");
          activeClassAddRemove('.info-left-heading1');
      }
  });

  $(document).ready(function(){    
      $(".info-left-heading1").click(function(){
          $(".menu-les-formulaires-container").toggleClass("over-menu");
          activeClassAddRemove('.info-left-heading1');
          classExistAndRemove('.menu-zone-patient-container','.info-right-heading1');
          classExistAndRemove('.menu-coin-des-petits-container','.info-left-heading3');
          classExistAndRemove('.menu-conseils-post-operatoires-container','.info-right-heading3');
      });
  });

  $(document).ready(function(){
      $(".info-right-heading1").click(function(){
          $(".menu-zone-patient-container").toggleClass("over-menu");
          activeClassAddRemove('.info-right-heading1');
          classExistAndRemove('.menu-les-formulaires-container','.info-left-heading1');
          classExistAndRemove('.menu-coin-des-petits-container','.info-left-heading3');
          classExistAndRemove('.menu-conseils-post-operatoires-container','.info-right-heading3');
      });
  });

  $(document).ready(function(){
      $(".info-left-heading3").click(function(){
          $(".menu-coin-des-petits-container").toggleClass("over-menu");
          activeClassAddRemove('.info-left-heading3');
          classExistAndRemove('.menu-zone-patient-container','.info-right-heading1');
          classExistAndRemove('.menu-les-formulaires-container','.info-left-heading1');
          classExistAndRemove('.menu-conseils-post-operatoires-container','.info-right-heading3');
      });
  });
  $(document).ready(function(){
      $(".info-right-heading3").click(function(){
          $(".menu-conseils-post-operatoires-container").toggleClass("over-menu");
          activeClassAddRemove('.info-right-heading3');
          classExistAndRemove('.menu-les-formulaires-container','.info-left-heading1');
          classExistAndRemove('.menu-coin-des-petits-container','.info-left-heading3');
          classExistAndRemove('.menu-zone-patient-container','.info-right-heading1');
      });
  });











