# INSTRUCTION

## Installation

### HomeBrew
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew update
	brew doctor // to make sure your system is ready to brew
	export PATH="/usr/local/bin:$PATH" // add the Homebrew location to your $PATH and source your bash

### Node
	brew install node // or download from node site : https://nodejs.org/en/
	cd ./wp-content/themes/infosignmedia/assets/build/
	npm install
	
### GULP.
	
	If you have previously installed a version of gulp globally, please run ... to make sure your old version doesn't collide with gulp-cli.
	
	npm rm --global gulp
	npm install --global gulp-cli
	
	// change de proxy name in gulpfile.js
	 var proxyName = "name-of-your-site.com";
	gulp
	
### Browser-sync

	npm install -g browser-sync
	
	browser-sync --version
	
	browser-sync start --proxy gulp.com --files "scss/*.scss, js/**/*.js, /*.php"
	
### DEPLOY

	npm install dploy -g
	
	dploy --help
	
	dploy --version