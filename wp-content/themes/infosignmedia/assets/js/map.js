 google.maps.event.addDomListener(window, 'load', init);
 var map;
 var marker;

 function init() {
    var mapOptions = {
        center: new google.maps.LatLng(mapArray[0][5], mapArray[0][6]),
        zoom: 17,
        zoomControl: true,
        disableDoubleClickZoom: true,
        mapTypeControl: true,
        scaleControl: false,
        scrollwheel: false,
        panControl: false,
        streetViewControl: false,
        draggable: false,
        overviewMapControl: false,
        disableDefaultUI: false,
        animation: google.maps.Animation.DROP,
        overviewMapControlOptions: {
            opened: false
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: JSON.parse(mapStyle), 
    }
    var mapElement = document.getElementById('map');
    if(mapElement != null){
        var map = new google.maps.Map(mapElement, mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var locations = mapArray;
        for (i = 0; i < locations.length; i++) {

         name = (locations[i][0] == 'undefined')?'':name = locations[i][0]; 
         description = (locations[i][1] == 'undefined')?'':locations[i][1]; 
         telephone = (locations[i][2] == 'undefined')?'':locations[i][2];
         email  = (locations[i][3] == 'undefined')?'':locations[i][3];
         web = (locations[i][4] == 'undefined')?'':locations[i][4];
         markericon = (locations[i][7] == 'undefined')?'':locations[i][7];
         latitude = (locations[i][5] == 'undefined')?'':locations[i][5];
         longitude = (locations[i][6] == 'undefined')?'':locations[i][6];

         marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng(locations[i][5], locations[i][6]),
            map: map,
            title: locations[i][0],
            desc: description,
            tel: telephone,
            email: email,
            web: web,
            draggable: true,
            animation: google.maps.Animation.DROP,
        });

         marker.setDraggable(false);

         marker.addListener('click', function(){

            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
            var itinaryString = ($('html').attr('lang')[0] == 'f')?'Itinéraire':'Itinerary';
            var html = '<div id="content-map">'+
            '<h3>'+name+'</h3></br>'+
            '<a href="tel:'+telephone.replace(/\(|\)|\s*\-*/g, '', telephone)+'" class="num link">'+telephone+'</a></br></br>'+
            '<a style="font-weight: bold;" href="mailto:"'+email+'"">'+email+'</a>'+
            '<p>'+web+'</p>'+
            '<a class="itineraire" target="_blank" href="https://goo.gl/maps/54nHGW2y4d12">Itinéraire</a>'+
            '</div>';

            infowindow.setContent(html);
            infowindow.open(map, marker);
        });

         map.addListener('center_changed', function() {

            window.setTimeout(function() {
              map.panTo(marker.getPosition());
          }, 0);
        });
     }
 }
}


     





