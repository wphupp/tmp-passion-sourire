
    (function($){
        $(window).on("load",function(){
            $(".widget_nav_menu").mCustomScrollbar();
            $(".info_description").mCustomScrollbar();
            $(".single_information_description").mCustomScrollbar();
            $(".services_page_description").mCustomScrollbar();
            $(".single_services_description").mCustomScrollbar();
            $(".equipe-price-content").mCustomScrollbar();
            $(".equipe-description").mCustomScrollbar();
            $(".price-details-description").mCustomScrollbar();
            // $(".clinical-right-description-outer").mCustomScrollbar();
            $(".box_content").mCustomScrollbar();
            $(".postid-1080 .single_services_video").mCustomScrollbar();
            $(".menu_hamburger_scroll").mCustomScrollbar();
        });
    })(jQuery);
    
    /* hamburger */
        $(document).ready(function(){
            $(".hamburger").click(function(){
                $(".menu_title").toggleClass("open_menu");
                $("#site_content").toggleClass("menu_overlay");
                $(".site_logo").toggleClass("logo_overlay");
                 $("body").toggleClass("intro");
            });

            $(".lightbox_btn").click(function(){
                 $("body").addClass("lightbox_box_open");
            });
            $(".close").click(function(){
                 $("body").removeClass("lightbox_box_open");
            });
            $(".lightbox1").click(function(){
                 $("body").removeClass("lightbox_box_open");
            });

            /* Clinic Virtual Visit Lightbox */
            $(".virtual_visit_btn").click(function(){
                 $("body").addClass("lightbox_visit_open");
            });
            $(".close_visit").click(function(){
                 $("body").removeClass("lightbox_visit_open");
            });
            $(".virtual_visit_lightbox").click(function(){
                 $("body").removeClass("lightbox_visit_open");
            });


        }); 





(function($) {
    
    /*
    * We need to turn it into a function.
    * To apply the changes both on document ready and when we resize the browser.
    */
    
    function mediaSize() { 
        /* Set the matchMedia */
        if (window.matchMedia('(max-width: 1024px)').matches) {
        /* Changes when we reach the min-width  */
             $( ".info-right" ).after( $( ".info-left" ) );
             $( ".single-info-right" ).after( $( ".single-info-left" ) );
             $( ".equipe-price-details" ).appendTo( ".equipe_content_outer" );
             $("#equipe_content_outer").remove();
        } else {
        /* Reset for CSS changes – Still need a better way to do this! */
             $( ".info-left" ).after( $( ".info-right" ) );
             $( ".single-info-left" ).after( $( ".single-info-right" ) );
        }
        
    };
    
    /* Call the function */
  mediaSize();
  /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);  
    
})(jQuery);


(function($) {
    
    /*
    * We need to turn it into a function.
    * To apply the changes both on document ready and when we resize the browser.
    */
    
    function mediaSize() { 
        /* Set the matchMedia */
        if (window.matchMedia('(max-width: 767px)').matches) {
        /* Changes when we reach the min-width  */
            $( ".rightside-content" ).after( $( ".leftside-content" ) );
        } else {
        /* Reset for CSS changes – Still need a better way to do this! */
             $( ".leftside-content" ).after( $( ".rightside-content" ) );
        }
    };
    
    /* Call the function */
  mediaSize();
  /* Attach the function to the resize event listener */
    window.addEventListener('resize', mediaSize, false);  
    
})(jQuery);

 /* FancyBox Clinic Image  */
$(document).ready(function() {
  $(".fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none'
  });
});
$(".wpb_image_grid_ul li a").addClass("fancybox");
$('.wpb_image_grid_ul li a').attr('rel', 'gallery1')




/* Menu Fixed on Scroll */


// jQuery("document").ready(function($){

//   var nav = $('.header_top_outer');

//   $(window).scroll(function () {
//     if ($(this).scrollTop() > 50) {
//       nav.addClass("onscroll_fixed");
//     } else {
//       nav.removeClass("onscroll_fixed");
//     }
//   });

// });



 /* Front Page Lightbox JS  */

$(document).ready(function(){
 
$('.lightbox_btn').click(function(){
    $('.light_main').animate({'opacity':'.50'}, 300, 'linear');
    $('.light_main').animate({'opacity':'1.00'}, 300, 'linear');
    $('.light_main').css('display', 'block');

});

$('.close').click(function(){
    close_box();
});


$('.lightbox1').click(function(){
    close_box();
});

});

function close_box()
{
$('.light_main').animate({'opacity':'0'}, 300, 'linear', function(){
    $('.light_main').css('display', 'none');
});
}


/* Clinic Lightbox In Mobile Responsive */
$(document).ready(function(){
 
$('.virtual_visit_btn').click(function(){
    $('.virtual_visit_outer').animate({'opacity':'.50'}, 300, 'linear');
    $('.virtual_visit_outer').animate({'opacity':'1.00'}, 300, 'linear');
    $('.virtual_visit_outer').css('display', 'block');

});

$('.close_visit').click(function(){
    close_box_visit();
});


$('.virtual_visit_lightbox').click(function(){
    close_box_visit();
});

});

function close_box_visit()
{
$('.virtual_visit_outer').animate({'opacity':'0'}, 300, 'linear', function(){
    $('.virtual_visit_outer').css('display', 'none');
});
}




/* Front Page Lightbox JS End  */



