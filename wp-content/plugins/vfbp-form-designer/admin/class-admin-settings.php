<?php
/**
 * Class that handles all AJAX calls and admin settings
 *
 * This is called directly from vfb-pro/admin/class-addons.php and
 * vfb-pro/admin/class-ajax.php
 *
 * @since 3.0
 */
class VFB_Pro_Addon_Form_Designer_Admin_Settings {
	/**
	 * settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function settings( $data, $form_id ) {
		$design_enable    = isset( $data['form-design-enable'] ) ? $data['form-design-enable'] : '';

		// Font Options
		$font_text        = isset( $data['form-design']['design-font-text']        ) ? $data['form-design']['design-font-text']        : 'Helvetica';
		$font_size        = isset( $data['form-design']['design-font-size']        ) ? $data['form-design']['design-font-size']        : '14';

		// Label Styles
		$label_style      = isset( $data['form-design']['design-label-style']      ) ? $data['form-design']['design-label-style']      : 'bold';
		$label_spacing    = isset( $data['form-design']['design-label-spacing']    ) ? $data['form-design']['design-label-spacing']    : '5';
		$label_color      = isset( $data['form-design']['design-label-color']      ) ? $data['form-design']['design-label-color']      : '#444';

		// Input Styles
		$input_text_color    = isset( $data['form-design']['design-input-text-color']    ) ? $data['form-design']['design-input-text-color']    : '#555';
		$input_bg_color      = isset( $data['form-design']['design-input-bg-color']      ) ? $data['form-design']['design-input-bg-color']      : '#fff';
		$placeholder_color   = isset( $data['form-design']['design-placeholder-color']   ) ? $data['form-design']['design-placeholder-color']   : '#777';
		$input_border_width  = isset( $data['form-design']['design-input-border-width']  ) ? $data['form-design']['design-input-border-width']  : '1';
		$input_border_style  = isset( $data['form-design']['design-input-border-style']  ) ? $data['form-design']['design-input-border-style']  : 'solid';
		$input_border_color  = isset( $data['form-design']['design-input-border-color']  ) ? $data['form-design']['design-input-border-color']  : '#ccc';
		$input_border_radius = isset( $data['form-design']['design-input-border-radius'] ) ? $data['form-design']['design-input-border-radius'] : '4';
		$input_focus_color   = isset( $data['form-design']['design-input-focus-color']   ) ? $data['form-design']['design-input-focus-color']   : '#66afe9';
		$input_focus_shadow  = isset( $data['form-design']['design-input-focus-shadow']  ) ? $data['form-design']['design-input-focus-shadow']  : true;

		// Descriptions
		$desc_text_color         = isset( $data['form-design']['design-desc-text-color']         ) ? $data['form-design']['design-desc-text-color']         : '#777';
		$desc_vertical_margin    = isset( $data['form-design']['design-desc-vertical-margin']    ) ? $data['form-design']['design-desc-vertical-margin']    : '5';
		$desc_horizontal_margin  = isset( $data['form-design']['design-desc-horizontal-margin']  ) ? $data['form-design']['design-desc-horizontal-margin']  : '0';
		$desc_vertical_padding   = isset( $data['form-design']['design-desc-vertical-padding']   ) ? $data['form-design']['design-desc-vertical-padding']   : '0';
		$desc_horizontal_padding = isset( $data['form-design']['design-desc-horizontal-padding'] ) ? $data['form-design']['design-desc-horizontal-padding'] : '0';

		// Validation Styles
		$invalid_text_color   = isset( $data['form-design']['design-invalid-text-color']   ) ? $data['form-design']['design-invalid-text-color']   : '#a94442';
		$invalid_border_color = isset( $data['form-design']['design-invalid-border-color'] ) ? $data['form-design']['design-invalid-border-color'] : '#a94442';
		$valid_text_color     = isset( $data['form-design']['design-valid-text-color']     ) ? $data['form-design']['design-valid-text-color']     : '#3c763d';
		$valid_border_color   = isset( $data['form-design']['design-valid-border-color']   ) ? $data['form-design']['design-valid-border-color']   : '#3c763d';

		// Buttons
		$btn_text_color         = isset( $data['form-design']['design-btn-text-color']         ) ? $data['form-design']['design-btn-text-color']         : '#fff';
		$btn_bg_color           = isset( $data['form-design']['design-btn-bg-color']           ) ? $data['form-design']['design-btn-bg-color']           : '#337ab7';
		$btn_font_size          = isset( $data['form-design']['design-btn-font-size']          ) ? $data['form-design']['design-btn-font-size']          : '14';
		$btn_font_weight        = isset( $data['form-design']['design-btn-font-weight']        ) ? $data['form-design']['design-btn-font-weight']        : 'normal';
		$btn_border_width       = isset( $data['form-design']['design-btn-border-width']       ) ? $data['form-design']['design-btn-border-width']       : '1';
		$btn_border_style       = isset( $data['form-design']['design-btn-border-style']       ) ? $data['form-design']['design-btn-border-style']       : 'solid';
		$btn_border_color       = isset( $data['form-design']['design-btn-border-color']       ) ? $data['form-design']['design-btn-border-color']       : '#2e6da4';
		$btn_border_radius      = isset( $data['form-design']['design-btn-border-radius']      ) ? $data['form-design']['design-btn-border-radius']      : '4';
		$btn_hover_text_color   = isset( $data['form-design']['design-btn-hover-text-color']   ) ? $data['form-design']['design-btn-hover-text-color']   : '#fff';
		$btn_hover_bg_color     = isset( $data['form-design']['design-btn-hover-bg-color']     ) ? $data['form-design']['design-btn-hover-bg-color']     : '#286090';
		$btn_hover_border_color = isset( $data['form-design']['design-btn-hover-border-color'] ) ? $data['form-design']['design-btn-hover-border-color'] : '#204d74';

		// CSS
		$css = isset( $data['form-design']['design-css'] ) ? $data['form-design']['design-css'] : '';

		$copy_url = add_query_arg(
			array(
				'page'    => 'vfb-pro',
				'action'  => 'vfbp-form-desinger-copy-settings',
				'form-id' => $form_id,
				'width'   => 600,
				'height'  => 550,
			),
			wp_nonce_url( admin_url( 'admin-ajax.php' ), 'vfbp_designer_copy_settings' )
		);
	?>
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row">
					<label for="design-enable"><?php _e( 'Enable Form Design' , 'vfbp-form-designer' ); ?></label>
				</th>
				<td>
					<fieldset>
						<label>
							<input type="hidden" name="settings[form-design-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
							<input type="checkbox" name="settings[form-design-enable]" id="design-enable" value="1"<?php checked( $design_enable, 1 ); ?> /> <?php _e( "Enable custom design for this form.", 'vfbp-form-designer' ); ?>
						</label>
					</fieldset>
				</td>
			</tr>
		</tbody>
		<tbody class="vfb-form-design-settings<?php echo !empty( $design_enable ) ? ' active' : ''; ?>">
			<tr valign="top">
				<th scope="row">
				</th>
				<td>
					<div class="vfb-accordion-container">
						<ul class="outer-border">
							<!-- !Font Options -->
							<li class="vfb-control-section vfb-accordion-section open">
								<h3 class="vfb-accordion-section-title">
									<?php _e( 'Font Options', 'vfbp-form-designer' ); ?>
								</h3>
								<div class="vfb-accordion-section-content">
									<label>
										<span class="vfb-control-title"><?php _e( 'Font Family', 'vfbp-form-designer' ); ?></span>
										<select name="settings[form-design][design-font-text]" id="font-text">
											<?php $this->font_select( $font_text ); ?>
										</select>
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Font Size', 'vfbp-form-designer' ); ?></span>
										<select name="settings[form-design][design-font-size]" id="font-size">
											<option value="8"<?php selected( '8', $font_size ); ?>>8</option>
											<option value="9"<?php selected( '9', $font_size ); ?>>9</option>
											<option value="10"<?php selected( '10', $font_size ); ?>>10</option>
											<option value="11"<?php selected( '11', $font_size ); ?>>11</option>
											<option value="12"<?php selected( '12', $font_size ); ?>>12</option>
											<option value="13"<?php selected( '13', $font_size ); ?>>13</option>
											<option value="14"<?php selected( '14', $font_size ); ?>>14</option>
											<option value="15"<?php selected( '15', $font_size ); ?>>15</option>
											<option value="16"<?php selected( '16', $font_size ); ?>>16</option>
											<option value="17"<?php selected( '17', $font_size ); ?>>17</option>
											<option value="18"<?php selected( '18', $font_size ); ?>>18</option>
											<option value="19"<?php selected( '19', $font_size ); ?>>19</option>
											<option value="20"<?php selected( '20', $font_size ); ?>>20</option>
											<option value="21"<?php selected( '21', $font_size ); ?>>21</option>
											<option value="22"<?php selected( '22', $font_size ); ?>>22</option>
											<option value="23"<?php selected( '23', $font_size ); ?>>23</option>
											<option value="24"<?php selected( '24', $font_size ); ?>>24</option>
										</select> px
									</label>
								</div> <!-- .vfb-accordion-section-content -->
							</li>

							<!-- !Label Styles -->
							<li class="vfb-control-section vfb-control-radio vfb-accordion-section">
								<h3 class="vfb-accordion-section-title">
									<?php _e( 'Label Styles', 'vfbp-form-designer' ); ?>
								</h3>
								<div class="vfb-accordion-section-content">
									<span class="vfb-control-title"><?php _e( 'Label Style', 'vfbp-form-designer' ); ?></span>
									<label>
										<input type="radio" value="bold" name="settings[form-design][design-label-style]"<?php checked( $label_style, 'bold' ); ?> /> <?php _e( 'Bold', 'vfbp-form-designer' ); ?>
									</label>

									<br>

									<label>
										<input type="radio" value="normal" name="settings[form-design][design-label-style]"<?php checked( $label_style, 'normal' ); ?> /> <?php _e( 'Normal', 'vfbp-form-designer' ); ?>
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Label Spacing', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $label_spacing ); ?>" name="settings[form-design][design-label-margin]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Label Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-label-color]" id="color-label" class="vfb-color-picker" value="<?php esc_attr_e( $label_color ); ?>" data-default-color="#444" />
									</label>
								</div> <!-- .vfb-accordion-section-content -->
							</li>

							<!-- !Input Styles -->
							<li class="vfb-control-section vfb-control-radio vfb-accordion-section">
								<h3 class="vfb-accordion-section-title">
									<?php _e( 'Input Styles', 'vfbp-form-designer' ); ?>
								</h3>
								<div class="vfb-accordion-section-content">
									<label>
										<span class="vfb-control-title"><?php _e( 'Input Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-input-text-color]" id="input-text-color" class="vfb-color-picker" value="<?php esc_attr_e( $input_text_color ); ?>" data-default-color="#555" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Input Background Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-input-bg-color]" id="input-bg-color" class="vfb-color-picker" value="<?php esc_attr_e( $input_bg_color ); ?>" data-default-color="#fff" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Placeholder Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-placeholder-color]" id="placeholder-color" class="vfb-color-picker" value="<?php esc_attr_e( $placeholder_color ); ?>" data-default-color="#777" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Width', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $input_border_width ); ?>" name="settings[form-design][design-input-border-width]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Style', 'vfbp-form-designer' ); ?></span>
										<select name="settings[form-design][design-input-border-style]" id="border-style">
											<option value="solid"<?php selected( 'solid', $input_border_style ); ?>>solid</option>
											<option value="dashed"<?php selected( 'dashed', $input_border_style ); ?>>dashed</option>
											<option value="dotted"<?php selected( 'dotted', $input_border_style ); ?>>dotted</option>
											<option value="double"<?php selected( 'double', $input_border_style ); ?>>double</option>
											<option value="groove"<?php selected( 'groove', $input_border_style ); ?>>groove</option>
											<option value="ridge"<?php selected( 'ridge', $input_border_style ); ?>>ridge</option>
											<option value="inset"<?php selected( 'inset', $input_border_style ); ?>>inset</option>
											<option value="outset"<?php selected( 'outset', $input_border_style ); ?>>outset</option>
											<option value="none"<?php selected( 'none', $input_border_style ); ?>>none</option>
										</select>
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-input-border-color]" id="input-border-color" class="vfb-color-picker" value="<?php esc_attr_e( $input_border_color ); ?>" data-default-color="#ccc" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Radius', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $input_border_radius ); ?>" name="settings[form-design][design-input-border-radius]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Focus Border Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-input-focus-color]" id="input-focus-color" class="vfb-color-picker" value="<?php esc_attr_e( $input_focus_color ); ?>" data-default-color="#66afe9" />
									</label>

									<fieldset>
										<label>
											<span class="vfb-control-title"><?php _e( 'Focus Box Shadow', 'vfbp-form-designer' ); ?></span>
											<input type="hidden" name="settings[form-design][design-input-focus-shadow]" value="0" /> <!-- This sends an unchecked value to the meta table -->
											<input type="checkbox" name="settings[form-design][design-input-focus-shadow]" id="input-focus-shadow" value="1"<?php checked( $input_focus_shadow, 1 ); ?> /> <?php _e( "Display box shadow when input is in focus.", 'vfbp-form-designer' ); ?>
										</label>
									</fieldset>
								</div> <!-- .vfb-accordion-section-content -->
							</li>

							<!-- !Descriptions -->
							<li class="vfb-control-section vfb-control-radio vfb-accordion-section">
								<h3 class="vfb-accordion-section-title">
									<?php _e( 'Descriptions', 'vfbp-form-designer' ); ?>
								</h3>
								<div class="vfb-accordion-section-content">
									<label>
										<span class="vfb-control-title"><?php _e( 'Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-desc-text-color]" id="desc-text-color" class="vfb-color-picker" value="<?php esc_attr_e( $desc_text_color ); ?>" data-default-color="#777" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Vertical Margin', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $desc_vertical_margin ); ?>" name="settings[form-design][design-desc-vertical-margin]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Horizontal Margin', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $desc_horizontal_margin ); ?>" name="settings[form-design][design-desc-horizontal-margin]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Vertical Padding', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $desc_vertical_padding); ?>" name="settings[form-design][design-desc-vertical-padding]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Horizontal Padding', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $desc_horizontal_padding ); ?>" name="settings[form-design][design-desc-horizontal-padding]" /> px
									</label>
								</div> <!-- .vfb-accordion-section-content -->
							</li>

							<!-- !Validation Styles -->
							<li class="vfb-control-section vfb-control-radio vfb-accordion-section">
								<h3 class="vfb-accordion-section-title">
									<?php _e( 'Validation Styles', 'vfbp-form-designer' ); ?>
								</h3>
								<div class="vfb-accordion-section-content">
									<label>
										<span class="vfb-control-title"><?php _e( 'Invalid Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-invalid-text-color]" id="invalid-text-color" class="vfb-color-picker" value="<?php esc_attr_e( $invalid_text_color ); ?>" data-default-color="#a94442" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Invalid Border Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-invalid-border-color]" id="invalid-border-color" class="vfb-color-picker" value="<?php esc_attr_e( $invalid_border_color ); ?>" data-default-color="#a94442" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Valid Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-valid-text-color]" id="valid-text-color" class="vfb-color-picker" value="<?php esc_attr_e( $valid_text_color ); ?>" data-default-color="#3c763d" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Valid Border Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-valid-border-color]" id="valid-border-color" class="vfb-color-picker" value="<?php esc_attr_e( $valid_border_color ); ?>" data-default-color="#3c763d" />
									</label>
								</div> <!-- .vfb-accordion-section-content -->
							</li>

							<!-- !Buttons -->
							<li class="vfb-control-section vfb-control-radio vfb-accordion-section">
								<h3 class="vfb-accordion-section-title">
									<?php _e( 'Buttons', 'vfbp-form-designer' ); ?>
								</h3>
								<div class="vfb-accordion-section-content">
									<label>
										<span class="vfb-control-title"><?php _e( 'Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-btn-text-color]" id="btn-text-color" class="vfb-color-picker" value="<?php esc_attr_e( $btn_text_color ); ?>" data-default-color="#fff" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Background Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-btn-bg-color]" id="btn-bg-color" class="vfb-color-picker" value="<?php esc_attr_e( $btn_bg_color ); ?>" data-default-color="#337ab7" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Font Size', 'vfbp-form-designer' ); ?></span>
										<select name="settings[form-design][design-btn-font-size]" id="btn-font-size">
											<option value="8"<?php selected( '8', $btn_font_size ); ?>>8</option>
											<option value="9"<?php selected( '9', $btn_font_size ); ?>>9</option>
											<option value="10"<?php selected( '10', $btn_font_size ); ?>>10</option>
											<option value="11"<?php selected( '11', $btn_font_size ); ?>>11</option>
											<option value="12"<?php selected( '12', $btn_font_size ); ?>>12</option>
											<option value="13"<?php selected( '13', $btn_font_size ); ?>>13</option>
											<option value="14"<?php selected( '14', $btn_font_size ); ?>>14</option>
											<option value="15"<?php selected( '15', $btn_font_size ); ?>>15</option>
											<option value="16"<?php selected( '16', $btn_font_size ); ?>>16</option>
											<option value="17"<?php selected( '17', $btn_font_size ); ?>>17</option>
											<option value="18"<?php selected( '18', $btn_font_size ); ?>>18</option>
											<option value="19"<?php selected( '19', $btn_font_size ); ?>>19</option>
											<option value="20"<?php selected( '20', $btn_font_size ); ?>>20</option>
											<option value="21"<?php selected( '21', $btn_font_size ); ?>>21</option>
											<option value="22"<?php selected( '22', $btn_font_size ); ?>>22</option>
											<option value="23"<?php selected( '23', $btn_font_size ); ?>>23</option>
											<option value="24"<?php selected( '24', $btn_font_size ); ?>>24</option>
										</select> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Font Weight', 'vfbp-form-designer' ); ?></span>
										<select name="settings[form-design][design-btn-font-weight]" id="btn-font-weight">
											<option value="normal"<?php selected( 'normal', $btn_font_size ); ?>>8</option>
											<option value="bold"<?php selected( 'bold', $btn_font_size ); ?>>9</option>
											<option value="bolder"<?php selected( 'bolder', $btn_font_size ); ?>>10</option>
											<option value="lighter"<?php selected( 'lighter', $btn_font_size ); ?>>11</option>
											<option value="100"<?php selected( '100', $btn_font_size ); ?>>12</option>
											<option value="200"<?php selected( '200', $btn_font_size ); ?>>13</option>
											<option value="300"<?php selected( '300', $btn_font_size ); ?>>14</option>
											<option value="400"<?php selected( '400', $btn_font_size ); ?>>15</option>
											<option value="500"<?php selected( '500', $btn_font_size ); ?>>16</option>
											<option value="600"<?php selected( '600', $btn_font_size ); ?>>17</option>
											<option value="700"<?php selected( '700', $btn_font_size ); ?>>18</option>
											<option value="800"<?php selected( '800', $btn_font_size ); ?>>19</option>
											<option value="900"<?php selected( '900', $btn_font_size ); ?>>20</option>
										</select>
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Width', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $btn_border_width ); ?>" name="settings[form-design][design-btn-border-width]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Style', 'vfbp-form-designer' ); ?></span>
										<select name="settings[form-design][design-btn-border-style]" id="btn-border-style">
											<option value="solid"<?php selected( 'solid', $btn_border_style ); ?>>solid</option>
											<option value="dashed"<?php selected( 'dashed', $btn_border_style ); ?>>dashed</option>
											<option value="dotted"<?php selected( 'dotted', $btn_border_style ); ?>>dotted</option>
											<option value="double"<?php selected( 'double', $btn_border_style ); ?>>double</option>
											<option value="groove"<?php selected( 'groove', $btn_border_style ); ?>>groove</option>
											<option value="ridge"<?php selected( 'ridge', $btn_border_style ); ?>>ridge</option>
											<option value="inset"<?php selected( 'inset', $btn_border_style ); ?>>inset</option>
											<option value="outset"<?php selected( 'outset', $btn_border_style ); ?>>outset</option>
											<option value="none"<?php selected( 'none', $btn_border_style ); ?>>none</option>
										</select>
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-btn-border-color]" id="btn-border-color" class="vfb-color-picker" value="<?php esc_attr_e( $btn_border_color ); ?>" data-default-color="#2e6da4" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Border Radius', 'vfbp-form-designer' ); ?></span>
										<input type="number" value="<?php esc_attr_e( $btn_border_radius ); ?>" name="settings[form-design][design-btn-border-radius]" /> px
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Hover Text Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-btn-hover-text-color]" id="btn-hover-text-color" class="vfb-color-picker" value="<?php esc_attr_e( $btn_hover_text_color ); ?>" data-default-color="#fff" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Hover Background Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-btn-hover-bg-color]" id="btn-hover-bg-color" class="vfb-color-picker" value="<?php esc_attr_e( $btn_hover_bg_color ); ?>" data-default-color="#286090" />
									</label>

									<label>
										<span class="vfb-control-title"><?php _e( 'Hover Border Color', 'vfbp-form-designer' ); ?></span>
										<input type="text" name="settings[form-design][design-btn-hover-border-color]" id="btn-hover-border-color" class="vfb-color-picker" value="<?php esc_attr_e( $btn_hover_border_color ); ?>" data-default-color="#204d74" />
									</label>
								</div> <!-- .vfb-accordion-section-content -->
							</li>
						</ul>
					</div> <!-- .vfb-accordion-container -->
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="design-css"><?php _e( 'Custom CSS' , 'vfbp-form-designer' ); ?></label>
				</th>
				<td>
					<textarea id="design-css" name="settings[form-design][design-css]" cols="90" rows="15"><?php echo $css; ?></textarea>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="design-copy-settings"><?php _e( 'Copy Settings' , 'vfbp-form-designer' ); ?></label>
				</th>
				<td>
					<?php if ( empty( $design_enable ) ) : ?>
						<p style="color: red"><?php _e( 'You must save your custom design settings before you can copy them.', 'vfbp-form-designer' ); ?></p>
					<?php else : ?>
						<a id="vfb-design-custom-copy" href="<?php echo esc_url( $copy_url ); ?>" class="button thickbox" title="<?php _e( 'Copy Settings' , 'vfbp-form-designer'); ?>">
							<?php _e( 'Copy Settings' , 'vfbp-form-designer'); ?>
						</a>
					<?php endif; ?>
				</td>
			</tr>
		</tbody>
	</table>
	<?php
	}

	/**
	 * Helper for the font selection
	 *
	 * @access private
	 * @param mixed $setting
	 * @return void
	 */
	private function font_select( $setting ) {
	?>
		<optgroup label="<?php esc_attr_e( 'Sans Serif Web Safe Fonts', 'vfbp-form-designer' ); ?>">
			<option value="Arial"<?php selected( 'Arial', $setting ); ?>><?php _e( 'Arial', 'vfbp-form-designer' ); ?></option>
			<option value="Arial Black"<?php selected( 'Arial Black', $setting ); ?>><?php _e( 'Arial Black', 'vfbp-form-designer' ); ?></option>
			<option value="Arial Narrow"<?php selected( 'Arial Narrow', $setting ); ?>><?php _e( 'Arial Narrow', 'vfbp-form-designer' ); ?></option>
			<option value="Arial Rounded MT Bold"<?php selected( 'Arial Rounded MT Bold', $setting ); ?>><?php _e( 'Arial Rounded MT Bold', 'vfbp-form-designer' ); ?></option>
			<option value="Avant Garde"<?php selected( 'Avant Garde', $setting ); ?>><?php _e( 'Avant Garde', 'vfbp-form-designer' ); ?></option>
			<option value="Calibri"<?php selected( 'Calibri', $setting ); ?>><?php _e( 'Calibri', 'vfbp-form-designer' ); ?></option>
			<option value="Candara"<?php selected( 'Candara', $setting ); ?>><?php _e( 'Candara', 'vfbp-form-designer' ); ?></option>
			<option value="Century Gothic"<?php selected( 'Century Gothic', $setting ); ?>><?php _e( 'Century Gothic', 'vfbp-form-designer' ); ?></option>
			<option value="Franklin Gothic Medium"<?php selected( 'Franklin Gothic Medium', $setting ); ?>><?php _e( 'Franklin Gothic Medium', 'vfbp-form-designer' ); ?></option>
			<option value="Futura"<?php selected( 'Futura', $setting ); ?>><?php _e( 'Futura', 'vfbp-form-designer' ); ?></option>
			<option value="Geneva"<?php selected( 'Geneva', $setting ); ?>><?php _e( 'Geneva', 'vfbp-form-designer' ); ?></option>
			<option value="Gill Sans"<?php selected( 'Gill Sans', $setting ); ?>><?php _e( 'Gill Sans', 'vfbp-form-designer' ); ?></option>
			<option value="Helvetica"<?php selected( 'Helvetica', $setting ); ?>><?php _e( 'Helvetica', 'vfbp-form-designer' ); ?></option>
			<option value="Impact"<?php selected( 'Impact', $setting ); ?>><?php _e( 'Impact', 'vfbp-form-designer' ); ?></option>
			<option value="Lucida Grande"<?php selected( 'Lucida Grande', $setting ); ?>><?php _e( 'Lucida Grande', 'vfbp-form-designer' ); ?></option>
			<option value="Optima"<?php selected( 'Optima', $setting ); ?>><?php _e( 'Optima', 'vfbp-form-designer' ); ?></option>
			<option value="Segoe UI"<?php selected( 'Segoe UI', $setting ); ?>><?php _e( 'Segoe UI', 'vfbp-form-designer' ); ?></option>
			<option value="Tahoma"<?php selected( 'Tahoma', $setting ); ?>><?php _e( 'Tahoma', 'vfbp-form-designer' ); ?></option>
			<option value="Trebuchet MS"<?php selected( 'Trebuchet MS', $setting ); ?>><?php _e( 'Trebuchet MS', 'vfbp-form-designer' ); ?></option>
			<option value="Verdana"<?php selected( 'Verdana', $setting ); ?>><?php _e( 'Verdana', 'vfbp-form-designer' ); ?></option>
		</optgroup>

		<optgroup label="<?php esc_attr_e( 'Serif Web Safe Fonts', 'vfbp-form-designer' ); ?>">
			<option value="Big Caslon"<?php selected( 'Big Caslon', $setting ); ?>><?php _e( 'Big Caslon', 'vfbp-form-designer' ); ?></option>
			<option value="Bodini MT"<?php selected( 'Bodini MT', $setting ); ?>><?php _e( 'Bodini MT', 'vfbp-form-designer' ); ?></option>
			<option value="Book Antiqua"<?php selected( 'Book Antiqua', $setting ); ?>><?php _e( 'Book Antiqua', 'vfbp-form-designer' ); ?></option>
			<option value="Calisto MT"<?php selected( 'Calisto MT', $setting ); ?>><?php _e( 'Calisto MT', 'vfbp-form-designer' ); ?></option>
			<option value="Cambria"<?php selected( 'Cambria', $setting ); ?>><?php _e( 'Cambria', 'vfbp-form-designer' ); ?></option>
			<option value="Didot"<?php selected( 'Didot', $setting ); ?>><?php _e( 'Didot', 'vfbp-form-designer' ); ?></option>
			<option value="Garamond"<?php selected( 'Garamond', $setting ); ?>><?php _e( 'Garamond', 'vfbp-form-designer' ); ?></option>
			<option value="Georgia"<?php selected( 'Georgia', $setting ); ?>><?php _e( 'Georgia', 'vfbp-form-designer' ); ?></option>
			<option value="Goudy Old Style"<?php selected( 'Goudy Old Style', $setting ); ?>><?php _e( 'Goudy Old Style', 'vfbp-form-designer' ); ?></option>
			<option value="Hoefler Text"<?php selected( 'Hoefler Text', $setting ); ?>><?php _e( 'Hoefler Text', 'vfbp-form-designer' ); ?></option>
			<option value="Lucida Bright"<?php selected( 'Lucida Bright', $setting ); ?>><?php _e( 'Lucida Bright', 'vfbp-form-designer' ); ?></option>
			<option value="Palatino"<?php selected( 'Palatino', $setting ); ?>><?php _e( 'Palatino', 'vfbp-form-designer' ); ?></option>
			<option value="Perpetua"<?php selected( 'Perpetua', $setting ); ?>><?php _e( 'Perpetua', 'vfbp-form-designer' ); ?></option>
			<option value="Rockwell"<?php selected( 'Rockwell', $setting ); ?>><?php _e( 'Rockwell', 'vfbp-form-designer' ); ?></option>
			<option value="Rockwell Extra Bold"<?php selected( 'Rockwell Extra Bold', $setting ); ?>><?php _e( 'Rockwell Extra Bold', 'vfbp-form-designer' ); ?></option>
			<option value="Baskerville"<?php selected( 'Baskerville', $setting ); ?>><?php _e( 'Baskerville', 'vfbp-form-designer' ); ?></option>
			<option value="Times New Roman"<?php selected( 'Times New Roman', $setting ); ?>><?php _e( 'Times New Roman', 'vfbp-form-designer' ); ?></option>
		</optgroup>

		<optgroup label="<?php esc_attr_e( 'Monospaced Web Safe Fonts', 'vfbp-form-designer' ); ?>">
			<option value="Consolas"<?php selected( 'Consolas', $setting ); ?>><?php _e( 'Consolas', 'vfbp-form-designer' ); ?></option>
			<option value="Courier New"<?php selected( 'Courier New', $setting ); ?>><?php _e( 'Courier New', 'vfbp-form-designer' ); ?></option>
			<option value="Lucida Console"<?php selected( 'Lucida Console', $setting ); ?>><?php _e( 'Lucida Console', 'vfbp-form-designer' ); ?></option>
			<option value="Lucida Sans Typewriter"<?php selected( 'Lucida Sans Typewriter', $setting ); ?>><?php _e( 'Lucida Sans Typewriter', 'vfbp-form-designer' ); ?></option>
			<option value="Monaco"<?php selected( 'Monaco', $setting ); ?>><?php _e( 'Monaco', 'vfbp-form-designer' ); ?></option>
		</optgroup>

		<optgroup label="<?php esc_attr_e( 'Fantasy Web Safe Fonts', 'vfbp-form-designer' ); ?>">
			<option value="Copperplate"<?php selected( 'Copperplate', $setting ); ?>><?php _e( 'Copperplate', 'vfbp-form-designer' ); ?></option>
			<option value="Papryus"<?php selected( 'Papryus', $setting ); ?>><?php _e( 'Papryus', 'vfbp-form-designer' ); ?></option>
		</optgroup>

		<optgroup label="<?php esc_attr_e( 'Script Web Safe Fonts', 'vfbp-form-designer' ); ?>">
			<option value="Brush Script MT"<?php selected( 'Brush Script MT', $setting ); ?>><?php _e( 'Brush Script MT', 'vfbp-form-designer' ); ?></option>
		</optgroup>
	<?php
	}

	/**
	 * copy_settings function.
	 *
	 * @access public
	 * @return void
	 */
	public function copy_settings() {

		check_admin_referer( 'vfbp_designer_copy_settings' );

		$form_id = isset( $_GET['form-id'] ) ? absint( $_GET['form-id'] ) : 0;

		$vfbdb = new VFB_Pro_Data();
		$where = ' WHERE status = "publish" AND id != ' . $form_id;
		$forms = $vfbdb->get_all_forms( $where );
	?>
		<div>
			<p><?php _e( 'Select one or more forms to copy your settings to.', 'vfbp-form-designer' ); ?></p>

			<form id="vfbp-designer-copy-settings" class="media-upload-form type-form validate">
				<input type="hidden" name="form-id" value="<?php echo $form_id; ?>" />
				<?php
					wp_nonce_field( 'vfbp_designer_settings_save' );
				?>
				<?php
					foreach ( $forms as $form ) {
				        echo sprintf(
				        	'<label for="vfb-form-design-copy-%1$d">' .
				        	'<input type="checkbox" id="vfb-form-design-copy-%1$d" name="vfbp-copy-ids[]" value="%1$d" /> %1$d - %2$s' .
				        	'</label><br>',
							$form['id'],
							$form['title']
				        );
				    }

					submit_button(
						__( 'Copy', 'vfbp-form-designer' ),
						'primary',
						'' // leave blank so "name" attribute will not be added
					);
				?>
			</form>
		</div>
	<?php
		die(1);
	}

	/**
	 * copy_settings_save function.
	 *
	 * @access public
	 * @return void
	 */
	public function copy_settings_save() {

		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_POST['action'] ) && 'vfbp-form-designer-copy-settings-save' !== $_POST['action'] )
			return;

		parse_str( $_POST['data'], $data );

		$copy_ids = (array) $data['vfbp-copy-ids'];
		$form_id  = absint( $data['form-id'] );

		if ( empty( $copy_ids ) )
			return;

		$vfbdb = new VFB_Pro_Data();
		$meta  = $vfbdb->get_meta_by_id( $form_id );

		$design_enable   = isset( $meta['form-design-enable'] ) ? $meta['form-design-enable'] : '';
		$design_settings = isset( $meta['form-design']        ) ? $meta['form-design']        : '';

		if ( empty( $design_enable ) || empty( $design_settings ) )
			return;

		$design_settings = maybe_unserialize( $design_settings );

		if ( is_array( $copy_ids ) ) {
			foreach ( $copy_ids as $id ) {
				$vfbdb->update_metadata( $id, 'form-design-enable', $design_enable );
				$vfbdb->update_metadata( $id, 'form-design', $design_settings );
			}
		}

		die(1);
	}
}