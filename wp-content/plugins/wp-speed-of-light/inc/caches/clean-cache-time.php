<?php
//Based on some work of simple-cache
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class WpSoL_CleanCacheTime{
    public function __construct() {
        
    }
    public function set_action(){
        add_action( 'wpsol_purge_cache', array( $this, 'purge_cache' ) );
        add_action( 'init', array( $this, 'schedule_events' ) );
        add_filter( 'cron_schedules', array( $this, 'filter_cron_schedules' ) );
        }
//       set up schedule_events
    public function schedule_events() {
            $config = get_option('wpsol_optimization_settings');

            $timestamp = wp_next_scheduled( 'wpsol_purge_cache' );
            // Expire cache never
            if ( isset( $config['speed_optimization']['clean_cache'] ) && $config['speed_optimization']['clean_cache'] === 0 ) {
                    wp_unschedule_event( $timestamp, 'wpsol_purge_cache' );
                    return;
            }
            if ( ! $timestamp ) {
                    wp_schedule_event( time(), 'wpsol_cache', 'wpsol_purge_cache' );
            }
    }
//     * Unschedule events
        public function unschedule_events() {
                    $timestamp = wp_next_scheduled( 'wpsol_purge_cache' );

                    wp_unschedule_event( $timestamp, 'wpsol_purge_cache' );
            }
        /**
	 * Add custom cron schedule
	 */
	public function filter_cron_schedules( $schedules ) {

		$config = get_option('wpsol_optimization_settings');

		$interval = HOUR_IN_SECONDS;

		if ( ! empty( $config['speed_optimization']['clean_cache'] ) && $config['speed_optimization']['clean_cache'] > 0 ) {
			$interval = $config['speed_optimization']['clean_cache'] * 60;
		}

		$schedules['wpsol_cache'] = array(
			'interval' => apply_filters( 'wpsol_cache_purge_interval', $interval ),
			'display'  => esc_html__( 'WPSOL Cache Purge Interval', 'wp-speed-of-light' ),
		);

                return $schedules;
	}
//    /        a cache purse
    public function purge_cache() {
            $config = get_option('wpsol_optimization_settings');

            // Do nothing, caching is turned off
            if ( empty( $config['speed_optimization']['act_cache'] ) ) {
                    return;
            }
            WpSoL_Cache::factory()->wpsol_cache_flush();
            WpSoL_MinificationCache::clear_minification();
    }
    public static function factory() {

		static $instance;

		if ( ! $instance ) {
			$instance = new self();
			$instance->set_action();
		}

		return $instance;
	}
}

