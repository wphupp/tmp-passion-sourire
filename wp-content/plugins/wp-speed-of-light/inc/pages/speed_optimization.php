<?php 
$optimization = get_option('wpsol_optimization_settings');

//disabled cache mobile and tablet when other mobile plugin installed
$disabled = '';
if(file_exists(WP_PLUGIN_DIR.'/wp-mobile-detect/wp-mobile-detect.php') ||
   file_exists(WP_PLUGIN_DIR.'/wp-mobile-edition/wp-mobile-edition.php') ||
   file_exists(WP_PLUGIN_DIR.'/wptouch/wptouch.php') ||
   file_exists(WP_PLUGIN_DIR.'/wiziapp-create-your-own-native-iphone-app/wiziapp.php') ||
   file_exists(WP_PLUGIN_DIR.'/wordpress-mobile-pack/wordpress-mobile-pack.php')){
    $disabled = 'disabled';
}
?>
<?php if (isset($_REQUEST['save-settings']) && $_REQUEST['save-settings'] == 'success'): ?>
    <div id="message-save-settings" class="notice notice-success" style="margin: 10px 0px 10px 0;padding: 10px;"><strong><?php _e('Optimization settings saved', 'wp-speed-of-light'); ?></strong></div>
<?php endif; ?>
<div id="tabs-analysis">
    <ul class="tabs z-depth-1 cyan">
        <li class="tab" id="wpsol-speed-optimization"><a data-tab-id="tabSpeedOptimization" id="tab-tabSpeedOptimization" class="link-tab white-text waves-effect waves-light" href="#tab1"><?php _e('Speed optimization', 'wp-speed-of-light') ?></a></li>
        <li class="tab" id="wpsol-advanced-features"><a data-tab-id="tabAdvancedFeatures" id="tab-tabAdvancedFeatures" class="link-tab white-text waves-effect waves-light" href="#tab2"><?php _e('Advanced optimization', 'wp-speed-of-light') ?></a></a></li>
    </ul>
    
    <div class="">
        <div id="tab1" class="tab-content cyan lighten-4 active">
            <div class="content-optimization wpsol-optimization">
                <form class="" method="post" action="">
                    <input type="hidden" name="action" value="wpsol_save_settings">
                    <?php wp_nonce_field('wpsol_speed_optimization','_wpsol_nonce');?>
                        <ul class="field">
                            <li>
                                <label for="active-cache" class="text speedoflight_tool" alt="<?php _e('Cache activation will speedup your website by pre-loading common page elements and database queries','wp-speed-of-light') ?>"><?php _e('Activate cache system', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization" >
                                    <label class="switch switch-optimization">
                                        <input type="checkbox" class="wpsol-optimization" id="active-cache" name="active-cache" value="<?php echo $optimization['speed_optimization']['act_cache']; ?>" <?php if(!empty($optimization) ){ if($optimization['speed_optimization']['act_cache'] == 1) echo 'checked="checked"';}  ?> >
                                        <div class="slider round"></div>
                                     </label>
                                </div>
                            </li>
                            <li>
                                <label for="clean-cache" class="text speedoflight_tool" alt="<?php _e('Automatically cleanup the cache stored each x minutes and generate a new version instantly','wp-speed-of-light') ?>"><?php _e('Clean cache after', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                   <input type="text" class="wpsol-optimization" id="clean-cache" name="clean-cache" value="<?php  echo $optimization['speed_optimization']['clean_cache'];  ?>">
                                   <label><?php _e('minutes', 'wp-speed-of-light')?></label>
                                 </div>  
                            </li>
                            <li>
                                <label for="cache-desktop" class="text speedoflight_tool" alt="<?php _e('Cache for desktop','wp-speed-of-light') ?>"><?php _e('Cache for desktop', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <select id="cache-desktop" name="cache-desktop" >
                                        <option value="1" <?php echo ($optimization['speed_optimization']['devices']['cache_desktop'] == 1)?'selected="selected"':'' ?>><?php _e('Activated','wp-speed-of-light') ?></option>
                                        <option value="2" <?php echo ($optimization['speed_optimization']['devices']['cache_desktop'] == 2)?'selected="selected"':'' ?>><?php _e('No cache for desktop','wp-speed-of-light') ?></option>
                                    </select>

                                </div>
                            </li>
                            <li>
                                <label for="cache-tablet" class="text speedoflight_tool" alt="<?php _e('Cache for tablet','wp-speed-of-light') ?>"><?php _e('Cache for tablet', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <select id="cache-tablet" name="cache-tablet" <?php echo $disabled; ?>>
                                        <option value="1" <?php echo ($optimization['speed_optimization']['devices']['cache_tablet'] == 1)?'selected="selected"':'' ?>><?php _e('Automatic (same as desktop)','wp-speed-of-light') ?></option>
                                        <option value="2" <?php echo ($optimization['speed_optimization']['devices']['cache_tablet'] == 2)?'selected="selected"':'' ?>><?php _e('Specific tablet cache','wp-speed-of-light') ?></option>
                                        <option value="3" <?php echo ($optimization['speed_optimization']['devices']['cache_tablet'] == 3)?'selected="selected"':'' ?>><?php _e('No cache for tablet','wp-speed-of-light') ?></option>
                                    </select>

                                </div>
                            </li>
                            <li>
                                <label for="cache-mobile" class="text speedoflight_tool" alt="<?php _e('Cache for mobile','wp-speed-of-light') ?>"><?php _e('Cache for mobile', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <select id="cache-mobile" name="cache-mobile" <?php echo $disabled; ?>>
                                        <option value="1" <?php echo ($optimization['speed_optimization']['devices']['cache_mobile'] == 1)?'selected="selected"':'' ?>><?php _e('Automatic (same as desktop)','wp-speed-of-light') ?></option>
                                        <option value="2" <?php echo ($optimization['speed_optimization']['devices']['cache_mobile'] == 2)?'selected="selected"':'' ?>><?php _e('Specific mobile cache','wp-speed-of-light') ?></option>
                                        <option value="3" <?php echo ($optimization['speed_optimization']['devices']['cache_mobile'] == 3)?'selected="selected"':'' ?>><?php _e('No cache for mobile','wp-speed-of-light') ?></option>
                                    </select>

                                </div>
                            </li>
                            <li>
                                <label for="active-compression" class="text speedoflight_tool" alt="<?php _e('Gzip compression is a form of HTTP compression. It makes text files smaller so they are transferred faster to your visitors and therefore speeds up your entire website','wp-speed-of-light') ?>"><?php _e('Activate Gzip compression', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch ">
                                        <input type="checkbox" class="wpsol-optimization" id="active-compression" name="active-compression" value="<?php echo $optimization['speed_optimization']['act_compression']; ?>" <?php if(!empty($optimization) ){ if($optimization['speed_optimization']['act_compression'] == 1) echo 'checked="checked"';}  ?>>
                                        <div class="slider round"></div>
                                     </label>
                                </div>
                            </li>
                            <li>
                                <label for="add-expires" class="text speedoflight_tool" alt="<?php _e('Expires headers gives instruction to the browser whether it should request a specific file from the server or whether they should grab it from the browser\'s cache (it’s faster).','wp-speed-of-light') ?>"><?php _e('Add expire headers', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch ">
                                        <input type="checkbox" class="wpsol-optimization" id="add-expires" name="add-expires" value="<?php echo $optimization['speed_optimization']['add_expires']; ?>" <?php if(!empty($optimization) ){ if($optimization['speed_optimization']['add_expires'] == 1) echo 'checked="checked"';}  ?>>
                                        <div class="slider round"></div>
                                     </label>
                                </div>
                            </li>
                        </ul>
                    <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>" class="btn waves-effect waves-light" id="speed-optimization"/>
                </form>
            </div>
        </div>
 
        <div id="tab2" class="tab-content cyan lighten-4">
             <div class="content-optimization wpsol-optimization">
                <form class="" method="post">
                    <input type="hidden" name="action" value="wpsol_save_minification">
                    <?php wp_nonce_field('wpsol_advanced_optimization','_wpsol_nonce');?>
                    <div class="text-intro">
                        <blockquote><i><?php _e('This features may not be applicable to all WordPress websites depending of of the server and the plugin used. Please run a full test on your website before considering keeping those options to Yes.','wp-speed-of-light') ?></i></blockquote>
                    </div>
                        <ul class="field">
                            <li>
                                <label for="html-minification" class="text speedoflight_tool" alt="<?php _e('Minification refers to the process of removing unnecessary or redundant data without affecting how the resource is processed by the browser - e.g. code comments and formatting, removing unused code, using shorter variable and function names, and so on','wp-speed-of-light') ?>"><?php _e('HTML minification', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch switch-optimization">
                                        <input type="checkbox" class="wpsol-minification" id="html-minification" name="html-minification" value="<?php echo $optimization['advanced_features']['html_minification']; ?>" <?php if(!empty($optimization) && $optimization['advanced_features']['html_minification'] == 1) echo 'checked="checked"';  ?>>
                                        <div class="slider round"></div>
                                     </label>
                                </div>
                            </li>
                            <li>
                                <label for="css-minification" class="text speedoflight_tool" alt="<?php _e('Minification refers to the process of removing unnecessary or redundant data without affecting how the resource is processed by the browser - e.g. code comments and formatting, removing unused code, using shorter variable and function names, and so on','wp-speed-of-light') ?>"><?php _e('CSS minification', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch ">
                                        <input type="checkbox" class="wpsol-minification" id="css-minification" name="css-minification" value="<?php echo $optimization['advanced_features']['css_minification']; ?>" <?php if(!empty($optimization) && $optimization['advanced_features']['css_minification'] == 1) echo 'checked="checked"';  ?>>
                                        <div class="slider round"></div>
                                     </label>
                                </div>
                            </li>
                            <li>
                                <label for="js-minification" class="text speedoflight_tool" alt="<?php _e('Minification refers to the process of removing unnecessary or redundant data without affecting how the resource is processed by the browser - e.g. code comments and formatting, removing unused code, using shorter variable and function names, and so on','wp-speed-of-light') ?>"><?php _e('JS minification', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch ">
                                        <input type="checkbox" class="wpsol-minification" id="js-minification" name="js-minification" value="<?php echo $optimization['advanced_features']['js_minification']; ?>" <?php if(!empty($optimization) && $optimization['advanced_features']['js_minification'] == 1) echo 'checked="checked"';  ?>>
                                        <div class="slider round"></div>
                                     </label>
                                </div>
                            </li>
                            <li>
                                <label for="cssGroup-minification" class="text speedoflight_tool" alt="<?php _e('Grouping several CSS files into a single file will minimize the HTTP requests number. Use with caution and test your website, it may generates conflicts','wp-speed-of-light') ?>"><?php _e('Group CSS', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch ">
                                        <input type="checkbox" class="wpsol-minification" id="cssGroup-minification" name="cssgroup-minification" value="<?php echo $optimization['advanced_features']['js_minification']; ?>" <?php if(!empty($optimization) && $optimization['advanced_features']['cssgroup_minification'] == 1) echo 'checked="checked"';  ?>>
                                        <div class="slider round"></div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <label for="jsGroup-minification" class="text speedoflight_tool" alt="<?php _e('Grouping several Javascript files into a single file will minimize the HTTP requests number. Use with caution and test your website, it may generates conflicts','wp-speed-of-light') ?>"><?php _e('Group JS', 'wp-speed-of-light')?></label>
                                <div class="switch-optimization">
                                    <label class="switch ">
                                        <input type="checkbox" class="wpsol-minification" id="jsGroup-minification" name="jsgroup-minification" value="<?php echo $optimization['advanced_features']['js_minification']; ?>" <?php if(!empty($optimization) && $optimization['advanced_features']['jsgroup_minification'] == 1) echo 'checked="checked"';  ?>>
                                        <div class="slider round"></div>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    <input type="submit" value="<?php _e('Save settings', 'wp-speed-of-light'); ?>" class="btn waves-effect waves-light" id="advanced-features"/>
                </form>
            </div>
        </div>
    </div>
</div>
