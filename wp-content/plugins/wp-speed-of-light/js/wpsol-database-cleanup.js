jQuery(document).ready(function($){
    jQuery('.speedoflight_tool').qtip({
        content: {
            attr: 'alt'
        },
        position: {
            my: 'top left',
            at: 'bottom bottom'
        },
        style: {
            tip: {
                corner: true,
            },
            classes: 'speedoflight-qtip qtip-rounded speedoflight-qtip-dashboard'
        },
        show: 'hover',
        hide: {
            fixed: true,
            delay: 10
        }

    }); 
    
    $('input[name="all_control"]').click(function(){
       var checked = $(this).is(':checked');
       if(checked == true){
           $(".clean-data").prop("checked",true);
       }else{
           $(".clean-data").prop("checked",false);
       }
    });
    $(".clean-data").click(function(){
        var checked = $(this).is(':checked');
        if(checked == false){
            $('input[name="all_control"]').prop('checked',false);
        }
    });
});