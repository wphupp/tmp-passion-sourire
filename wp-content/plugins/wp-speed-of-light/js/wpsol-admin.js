jQuery(document).ready(function($){

    $('#wpsol_activate').click(function(e){
        e.preventDefault();
        var activate = 'activate';
         $.ajax({
            dataType : 'json',
            method : 'POST',
            url : ajaxurl,
            data: {
                'action':'activate_button',
                'datas': activate
            },
            success: function(){
                window.location.href = "admin.php?page=wpsol_dashboard";
            }
        });
    });

    $('#wpsol_activate_cache_link').click(function(e){
        e.preventDefault();
        var active_cache = 'active_cache';
        $.ajax({
            dataType : 'json',
            method : 'POST',
            url : ajaxurl,
            data: {
                'action':'wpsol_activate_cache_link',
                'active_cache': active_cache
            },
            success: function(){
                window.location.href = "admin.php?page=wpsol_speed_optimization";
            }
        });
    });
});