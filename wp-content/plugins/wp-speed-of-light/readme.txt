=== WP Speed of Light ===
Contributors: JoomUnited
Tags: cache, caching, performance, speed test, performance test, wp-cache, cdn, combine, compress, speed plugin, database cache, deflate, webpagetest, gzip, http compression, js cache, minify, optimize, optimizer, page cache, performance, speed, expire headers, mobile cache
Requires at least: 4.5
Tested up to: 4.7.3
Stable tag: 1.3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WP Speed of Light is a WordPress speedup plugin and load time testing. Cache, Gzip compression, minify, database optimization tools

== Description ==

WordPress does not have a system for speed optimization by default. That's why you need a powerful plugin that speed up WordPress with technical and advanced features, yet accessible to everyone. 
WP Speed of Light starts with a cache and Gzip compression system, plus, you got a resource minification tool, a database cleanup system, an htaccess optimization and an auto cache cleaner.

When it comes to performance loading time, it require some test to identify witch plugin, theme, page may require more attention.
WP Speed of Light also includes a speed loading test and compare, and a database query speed test.


**<a href="https://www.joomunited.com/wordpress-products/wp-speed-of-light" rel="friend">More information and feature details here!</a>**

<a href="https://addendio.com/try-plugin/?slug=wp-speed-of-light" rel="friend">Still not sure! test the plugin live on addendio sandbox demo website</a>


= Video demo: =
[vimeo https://vimeo.com/186057806]

= WP Speed of Light; speed optimization features: =

* Generate static cache
* Selective or global cache for Desktop, Tablet and Mobile
* Activate Gzip compression
* Minify resources: HTML, CSS, JS
* Add Expire Headers (browser cache)
* Database optimization, clean: post revision, auto draft content, trashed content, spam comments, trackbacks and pingbacks, transients options
* Automatic clean cache on interval and cache cleaner button
* Automatic clean cache on content saving
* WooCommerce dedicated integration
* WordPress multisite compatible

= WP Speed of Light speed testing: =

* Page loading time, first load
* Page loading time, second load
* First byte time
* Time to start rendering
* Element served from cache in %
* Elements compressed with Gzip in %
* Percent on compressed images
* Run up to 200 page test per day (based on WebPageTest.org)
* Page builders compatibility: ACF, DIVI Builder, Beaver Builder, Site Origine, Themify builder, Live Composer, Elementor

Give a comfortable navigation experience to your users and get a better search engine rank!

Main plugins from JoomUnited:

* WP Media Folder: <a href="https://www.joomunited.com/wordpress-products/wp-media-folder" rel="friend"> https://www.joomunited.com/wordpress-products/wp-media-folder</a>
* WP File Download: <a href="https://www.joomunited.com/wordpress-products/wp-table-manager" rel="friend"> https://www.joomunited.com/wordpress-products/wp-table-manager</a>
* WP Table Manager: <a href="https://www.joomunited.com/wordpress-products/wp-file-download" rel="friend">https://www.joomunited.com/wordpress-products/wp-file-download</a>
* WP Meta SEO: <a href="https://www.joomunited.com/wordpress-products/wp-meta-seo" rel="friend">https://www.joomunited.com/wordpress-products/wp-meta-seo</a>
* WP Latest Posts: <a href="https://www.joomunited.com/wordpress-products/wp-latest-posts" rel="friend">https://www.joomunited.com/wordpress-products/wp-latest-posts</a>

= Support =

Before leaving a review >> Feel free to ask questions here in the support section, we reply to every question!
A dedicated private ticket support is also available on the website.

* Product page on the <a href="https://www.joomunited.com/wordpress-products/wp-speed-of-light" rel="friend">**[JoomUnited website]</a>

* Detailed documentation on the <a href="https://www.joomunited.com/documentation/wp-speed-of-light-documentation" rel="friend">**[JoomUnited documentation section]</a>

== Installation ==

= To install the automatically: =
* Through WordPress admin, use the menu: Plugin > Add new
* Search for WP Speed of Light
* Click on install then click activate link

= To install the plugin manually: =
* Download and unzip the plugin wp-speed-of-light.zip
* Upload the /wp-speed-of-light directory to /wp-content/plugins/
* Activate the plugin through the 'Plugins' menu in WordPress
* Use the WP Speed of Light left menu

Once the plugin is installed, open the admin left menu menu.

== Frequently Asked Questions ==

= Is WP Speed of Light 3rd party compatible? =
It is, of course we can't test with all dynamic element yet you have some options to exclude some specific URL from optimization. Feel free to send us compatibility request on our pre-sale forum or on the plugin directory support forum.

= Is WP Speed of Light free? =
Yes totally free of charge, fell free to install, update and even ask us questions on the WordPress plugin directory forum, if needed.

== Screenshots ==

1. Main dashboard of the plugin with speed optimization check
1. Generate static cache, Gzip compression, minification, browser cache
1. Test the speed of your WordPress pages to identify performance issues
1. Test the database queries to identify performance issues
1. Simple optimization settings, optimize in no time
1. Automatic and manual cache cleaner

== Changelog ==

= 1.3.0 =
* Add : Dashboard: Add the PHP version check and File Group activation
* Add : Option to serve cache for desktop, tablet and mobile: Global, Specific or No cache
* Add : Detect mobile plugin theme provider and disable cache automatically (WPtouch...)

= 1.2.0 =
* Add : Page builders compatibility : ACF, DIVI Builder, Beaver Builder, Site Origine, Themify builder, Live Composer, Elementor page builder
* Add : Update the settings with On/Off button instead of checkbox
* Add : Activate by default the clean cache button in the toolbar
* Add : Change presentation text for ImageRecycle integration

= 1.1.7 =
* Fix : Enhance PHP 5.2 plugin deactivation

= 1.1.6 =
* Add : Add builtin translation tool

= 1.1.5 =
* Fix : Support for russian language in URL exclusion tool
* Fix : Check Gzip request

= 1.1.4 =
* Fix : Read data from another server
* Fix : Disable cache for admin user
* Add : Replace Curl by wp_remote_get to get data

= 1.1.3 =
* Fix : Add multisite compatibility: minify JS and CSS on multisite

= 1.1.2 =
* Fix : Remove header expiration code in .htaccess when plugin is disabled

= 1.1.1 =
* Add : Option to group js and css (after minification)

= 1.1.0 =
* Add : WooCommerce dedicated integration
* Add : Image compression ImageRecycle integration
* Add : Clean cache on plugin settings save
* Fix : Security, use wp_nonce in admin forms

= 1.0.3 =
* Fix : PHP7 warning Missing argument
* Fix : Add blank line to wp-config.php when activate and deactivate plugin
* Fix : Remove everything from wp-config.php on uninstall
* Fix : Cache activation message on first activation only
* Fix : Gzip cache not activated on some servers

= 1.0.2 =
* Add : Display clean cache button in frontend toolbar
* Fix : Cache and purge cache on comments
* Fix : Analysis cache size

= 1.0.1 =
* Fix : White screen in first install

= 1.0.0 =
* Add : Initial release



== Upgrade Notice ==

Update through the automatic WordPress updater, all WP Speed of Light content will remain in place.


== Requirements ==

PHP 5.3+, PHP7 or 7.1 recommended for better performance, WordPress 4.5+

== Thanks to ==

We would like to thank you the developers of the following plugins that have inspired us with their code or UX

* Simple cache
* Autoptimize
* Query monitor plugin
