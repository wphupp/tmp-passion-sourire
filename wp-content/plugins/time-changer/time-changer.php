<?php
/**
* Plugin Name: Time Changer 
* Description: Help you handle your open hour on your website 
* Version: 1.0 
* Author: Samuel Cadieux 
* License: TC101 
**/ 
 
function register_cpt_heure_ouverture() {
 
    $labels = array(
        'name' => _x( "Heure d'ouvertures", 'heure_ouverture' ),
        'singular_name' => _x( "Heure d'ouverture", 'heure_ouverture'  ),
        'add_new' => _x( "Ajouter des heures d'ouverture", 'heure_ouverture' ),
        'add_new_item' => _x( "Ajouter mds heures d'ouverture", 'heure_ouverture' ),
        'edit_item' => _x( "Modifier les heures d'ouverture", 'heure_ouverture' ),
        'new_item' => _x( "Nouvelles heures d'ouverture", 'heure_ouverture' ),
        'view_item' => _x( "Voir mes heures d'ouverture", 'heure_ouverture' ),
        'search_items' => _x( "Cherchez des heures d'ouverture", 'heure_ouverture' ),
        'not_found' => _x( "Heures d'ouverture inexistantes", 'heure_ouverture' ),
        'not_found_in_trash' => _x( "Heures d'ouverture inexistantes", 'heure_ouverture' ),
        'parent_item_colon' => _x( "Heures d'ouverture parent", 'heure_ouverture' ),
        'menu_name' => _x( "Time Changer", 'heure_ouverture' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => '',
        'supports' => array( 'title'),
        'taxonomies' => array( 'genres' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-list-view',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );
 
    register_post_type( 'heure_ouverture', $args );
}

add_action( 'init', 'register_cpt_heure_ouverture' );
add_action('add_meta_boxes','init_metabox');

function init_metabox(){
    
        $label= "Heure d'ouverture";
   
  add_meta_box('heure_ouverture', $label, 'display_tc','heure_ouverture', 'normal','high');
}

function display_tc($post){
    
    $h0 = get_post_meta($post->ID,'_tc_meta_00',true);
    $hh0 = get_post_meta($post->ID,'_tc_meta_01',true);
    $h1 = get_post_meta($post->ID,'_tc_meta_10',true);
    $hh1 = get_post_meta($post->ID,'_tc_meta_11',true);
    $h2 = get_post_meta($post->ID,'_tc_meta_20',true);
    $hh2 = get_post_meta($post->ID,'_tc_meta_21',true);
    $h3 = get_post_meta($post->ID,'_tc_meta_30',true);
    $hh3 = get_post_meta($post->ID,'_tc_meta_31',true);
    $h4 = get_post_meta($post->ID,'_tc_meta_40',true);
    $hh4 = get_post_meta($post->ID,'_tc_meta_41',true);
    $h5 = get_post_meta($post->ID,'_tc_meta_50',true); 
    $hh5 = get_post_meta($post->ID,'_tc_meta_51',true); 
    $h6 = get_post_meta($post->ID,'_tc_meta_60',true); 
    $hh6 = get_post_meta($post->ID,'_tc_meta_61',true); 
    
    
        $tabDay = array('Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
        $closed = 'Fermé';
        $btwn = 'à';
   
               
    for($i=0; $i<count($tabDay);$i++)
    {
        $_h0 = ${'h'.$i};
        $_h1 = ${'hh'.$i};
        
        echo '<label style="width:85px;margin-right:10px;">'. $tabDay[$i] .'  </label>';
        echo '<select style="width:85px;" name="'.$i.'_open"><option'. selected( 'closed', $_h0, false ) .'>'.$closed.'</option> ';    
        for($hours=0; $hours<24; $hours++)
            { 
                for($mins=0; $mins<60; $mins+=15)
                {
                    $value = str_pad($hours,2,'0',STR_PAD_LEFT).'h'.str_pad($mins,2,'0',STR_PAD_LEFT);
                    $textValue = $value.'';
                    echo '<option ' . selected($textValue,  $_h0, false ) . ' value="'.$textValue.'">'.$value.'</option>'; 
                }
            } 
        
        
        echo '</select> '.$btwn.' <select style="width:85px;" name="'.$i.'_close"><option'. selected( 'closed',  $_h1, false ) .'>'.$closed.'</option>';
        for($hours=0; $hours<24; $hours++)
            {
                for($mins=0; $mins<60; $mins+=15)
                {
                    $value = str_pad($hours,2,'0',STR_PAD_LEFT).'h'.str_pad($mins,2,'0',STR_PAD_LEFT);
                    $textValue = $value.'';
                    echo '<option ' . selected($textValue, $_h1, false ) . ' value="'.$textValue.'">'.$value.'</option>';  
                }
            } 
         echo '</select><br>';
    }
}

add_action('save_post','save_metabox');
function save_metabox($post_id){
    if(isset($_POST['0_open']))
        update_post_meta($post_id, '_tc_meta_00', $_POST['0_open']);
    if(isset($_POST['0_close']))
        update_post_meta($post_id, '_tc_meta_01', $_POST['0_close']);
    if(isset($_POST['1_open']))
        update_post_meta($post_id, '_tc_meta_10', $_POST['1_open']);
    if(isset($_POST['1_close']))
        update_post_meta($post_id, '_tc_meta_11', $_POST['1_close']);
    if(isset($_POST['2_open']))
        update_post_meta($post_id, '_tc_meta_20', $_POST['2_open']);
    if(isset($_POST['2_close']))
        update_post_meta($post_id, '_tc_meta_21', $_POST['2_close']);
    if(isset($_POST['3_open']))
        update_post_meta($post_id, '_tc_meta_30', $_POST['3_open']);
    if(isset($_POST['3_close']))
        update_post_meta($post_id, '_tc_meta_31', $_POST['3_close']);
    if(isset($_POST['4_open']))
        update_post_meta($post_id, '_tc_meta_40', $_POST['4_open']);
    if(isset($_POST['4_close']))
        update_post_meta($post_id, '_tc_meta_41', $_POST['4_close']);
    if(isset($_POST['5_open']))
        update_post_meta($post_id, '_tc_meta_50', $_POST['5_open']);
    if(isset($_POST['5_close']))
        update_post_meta($post_id, '_tc_meta_51', $_POST['5_close']);
    if(isset($_POST['6_open']))
        update_post_meta($post_id, '_tc_meta_60', $_POST['6_open']);
    if(isset($_POST['6_close']))
        update_post_meta($post_id, '_tc_meta_61', $_POST['6_close']);
}

add_shortcode('open_hour', 'open_hour');
add_shortcode('open_today', 'open_today');

function open_hour($atts)
{    
    $r='';
    $a = shortcode_atts( array(
    'titre' => 'Défaut',
    'title' => 'Default',
    'class' => 'tc_week'
    ), $atts );
    
    if($a['titre']== '')
    {
        $a['titre'] = $a['title'];
    }
    
    global $wpdb;
    $postTitle = $a['titre'];
    $postId = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $postTitle . "'" );
    
    
        $tabDay = array('Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
        $btwn = 'à';


    $r .= "<ul style= 'list-style:none;margin-top: 0;margin-bottom: 0;'>";
    for($i=0; $i<6;$i++)
    {
        $o = get_post_meta($postId,'_tc_meta_'.$i.'0',true);
        $c = get_post_meta($postId,'_tc_meta_'.$i.'1',true);
        if($o == 'Closed' || $c == 'Closed' || $o == 'Fermé' || $c == 'Fermé')
        {
            
               $r .= "<li class='".$a['class']."'><strong> ".$tabDay[$i]." </strong>   Fermé </li>";
          
        }
        else{
             $r .= "<li class='".$a['class']."'><strong> ".$tabDay[$i]." </strong> ".$o . " ".$btwn." " .$c . "</li>";
        }
    }
    $r .= "</ul>";
    return $r;
}

function open_today($atts)
{
    $r='';
    
    
    
    $a = shortcode_atts( array(
    'titre' => 'Défaut',
    'title' => 'Default',
    'class' => 'tc_today'
    ), $atts );
    
    if($a['titre']== '')
    {
        $a['titre'] = $a['title'];
    }
    
    global $wpdb;
    $postTitle = $a['titre'];
    $postId = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $postTitle . "'" );
    $j = date('w');
    
    if($j == 0){$j =6;}
    else {$j = $j - 1;}
    
    $k = '_tc_meta_'.$j.'1';  
    $c = get_post_meta($postId,$k,true);
    
    
        if($c == 'Fermé' || $c == 'Closed')
        {
            $r .=  "<p class='".$a['class']."'>Fermé aujourd'hui</p>";     
        }else
            
        {
            $r .= "<p class='".$a['class']."'>Ouvert aujourd'hui jusqu'à ".$c."</p>";
        }
   
    
    return $r;
}