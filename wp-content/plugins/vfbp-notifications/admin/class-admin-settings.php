<?php
/**
 * Class that handles all AJAX calls and admin settings
 *
 * This is called directly from vfb-pro/admin/class-addons.php and
 * vfb-pro/admin/class-ajax.php
 *
 * @since 3.0
 */
class VFB_Pro_Addon_Notifications_Admin_Settings {

	/**
	 * mobile_settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function mobile_settings( $data ) {
		$cell_enable  = isset( $data['cell-enable']  ) ? $data['cell-enable']  : '';
		$cell_number  = isset( $data['cell-number']  ) ? $data['cell-number']  : '';
		$cell_carrier = isset( $data['cell-carrier'] ) ? $data['cell-carrier'] : '';
		$cell_message = isset( $data['cell-message'] ) ? $data['cell-message'] : '';
	?>
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="cell-enable"><strong><?php _e( 'Mobile Device' , 'vfbp-notifications'); ?></strong></label>
			</th>
			<td>
				<fieldset>
					<label>
						<input type="hidden" name="settings[cell-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
						<input type="checkbox" name="settings[cell-enable]" id="cell-enable" value="1"<?php checked( $cell_enable, 1 ); ?> /> <?php _e( "Enable Mobile Device notifications for this form.", 'vfbp-notifications' ); ?>
					</label>
				</fieldset>
			</td>
		</tr>
	</tbody>
	<tbody class="vfb-cell-settings<?php echo !empty( $cell_enable ) ? ' active' : ''; ?>">
		<tr valign="top">
			<th scope="row">
				<label for="cell-number"><?php _e( 'Your Cell Phone Number' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[cell-number]" id="cell-number" value="<?php esc_html_e( $cell_number ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="cell-carrier"><?php _e( 'Your Carrier' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select name="settings[cell-carrier]" id="cell-carrier">
					<option value=""<?php selected( $cell_carrier, '' ); ?>></option>
					<option value="Alltel"<?php selected( $cell_carrier, 'Alltel' ); ?>><?php _e( 'Alltel', 'vfbp-notifications' ); ?></option>
					<option value="ATT-iPhone"<?php selected( $cell_carrier, 'ATT-iPhone' ); ?>><?php _e( 'AT&T (iPhone)', 'vfbp-notifications' ); ?></option>
					<option value="ATT"<?php selected( $cell_carrier, 'ATT' ); ?>><?php _e( 'AT&T', 'vfbp-notifications' ); ?></option>
					<option value="BellAtlantic"<?php selected( $cell_carrier, 'BellAtlantic' ); ?>><?php _e( 'Bell Atlantic (message.bam.com)', 'vfbp-notifications' ); ?></option>
					<option value="BellCanada"<?php selected( $cell_carrier, 'BellCanada' ); ?>><?php _e( 'Bell Canada (bellmobility.ca)', 'vfbp-notifications' ); ?></option>
					<option value="BellMobilityCanada"<?php selected( $cell_carrier, 'BellMobilityCanada' ); ?>><?php _e( 'Bell Mobility (Canada - txt.bell.ca)', 'vfbp-notifications' ); ?></option>
					<option value="BellMobility"<?php selected( $cell_carrier, 'BellMobility' ); ?>><?php _e( 'Bell Mobility (txt.bellmobility.ca)', 'vfbp-notifications' ); ?></option>
					<option value="Boost"<?php selected( $cell_carrier, 'Boost' ); ?>><?php _e( 'Boost', 'vfbp-notifications' ); ?></option>
					<option value="CingularMe"<?php selected( $cell_carrier, 'CingularMe' ); ?>><?php _e( 'Cingular (cingularme.com)', 'vfbp-notifications' ); ?></option>
					<option value="Cingular"<?php selected( $cell_carrier, 'Cingular' ); ?>><?php _e( 'Cingular (mobile.mycingular.com)', 'vfbp-notifications' ); ?></option>
					<option value="Nextel"<?php selected( $cell_carrier, 'Nextel' ); ?>><?php _e( 'Nextel', 'vfbp-notifications' ); ?></option>
					<option value="Rogers"<?php selected( $cell_carrier, 'Rogers' ); ?>><?php _e( 'Rogers', 'vfbp-notifications' ); ?></option>
					<option value="Sprint"<?php selected( $cell_carrier, 'Sprint' ); ?>><?php _e( 'Sprint', 'vfbp-notifications' ); ?></option>
					<option value="T-Mobile"<?php selected( $cell_carrier, 'T-Mobile' ); ?>><?php _e( 'T-Mobile', 'vfbp-notifications' ); ?></option>
					<option value="Telus"<?php selected( $cell_carrier, 'Telus' ); ?>><?php _e( 'Telus', 'vfbp-notifications' ); ?></option>
					<option value="Verizon"<?php selected( $cell_carrier, 'Verizon' ); ?>><?php _e( 'Verizon', 'vfbp-notifications' ); ?></option>
					<option value="VirginMobile"<?php selected( $cell_carrier, 'VirginMobile' ); ?>><?php _e( 'Virgin Mobile (vmobl.com)', 'vfbp-notifications' ); ?></option>
					<option value="VirginMobileCA"<?php selected( $cell_carrier, 'VirginMobileCA' ); ?>><?php _e( 'Virgin Mobile CA (vmobile.ca)', 'vfbp-notifications' ); ?></option>
					<option value="Vodafone UK"<?php selected( $cell_carrier, 'Vodafone UK' ); ?>><?php _e( 'Vodafone UK', 'vfbp-notifications' ); ?></option>
					<option value="Other"<?php selected( $cell_carrier, 'Other' ); ?>><?php _e( 'Other', 'vfbp-notifications' ); ?></option>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="cell-message"><?php _e( 'Your Message' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<textarea name="settings[cell-message]" id="cell-message" class="large-text" rows="2"><?php echo $cell_message; ?></textarea>
			</td>
		</tr>
	</tbody>
	<?php
	}

	/**
	 * connect_mailchimp function.
	 *
	 * @access public
	 * @return void
	 */
	public function connect_mailchimp() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-connect-mailchimp' !== $_GET['action'] )
			return;

		$api_key = isset( $_GET['api']  ) ? esc_html( $_GET['api'] ) : '';
		$form_id = isset( $_GET['form'] ) ? absint( $_GET['form']  ) : '';

		$vfbdb = new VFB_Pro_Data();

		// If API key doesn't contain a dash, it's invalid
		if ( !strpos( $api_key, '-' ) ) {
			$response = array(
				'status'  => 0,
				'message' => __( 'Error: API key does not appear to be valid.', 'vfbp-notifications' ),
			);

			$vfbdb->update_metadata( $form_id, 'mailchimp-api-status', $response['status'] );
			$vfbdb->update_metadata( $form_id, 'mailchimp-api-message', $response['message'] );

			echo json_encode( $response );

			die(0);
		}

		list(, $datacentre) = explode( '-', $api_key );
		$api_endpoint = str_replace( '<dc>', $datacentre, 'https://<dc>.api.mailchimp.com/2.0' );

		$args = array(
			'apikey'	=> $api_key,
		);

		$request = wp_remote_post(
			"$api_endpoint/lists/list.json",
			array(
				'sslverify' => false,
				'body'      => $args,
			)
		);

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {

			$response = array(
				'status'  => 0,
				'message' => __( 'No MailChimp account found. Check the API key correctly.', 'vfbp-notifications' ),
			);

			$vfbdb->update_metadata( $form_id, 'mailchimp-api-status', $response['status'] );
			$vfbdb->update_metadata( $form_id, 'mailchimp-api-message', $response['message'] );

			echo json_encode( $response );

			die(0);
		}

		$response = array(
			'status'  => 1,
			'message' => __( 'VERIFIED', 'vfbp-notifications' ),
		);

		// Retrieve and set response
		$api_response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		$vfbdb->update_metadata( $form_id, 'mailchimp-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'mailchimp-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'mailchimp-api-data', $api_response );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * disconnect_mailchimp function.
	 *
	 * @access public
	 * @return void
	 */
	public function disconnect_mailchimp() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-disconnect-mailchimp' !== $_GET['action'] )
			return;

		$form_id = isset( $_GET['form'] ) ? absint( $_GET['form']  ) : '';

		$vfbdb = new VFB_Pro_Data();

		$response = array(
			'status'  => 0,
			'message' => __( 'DEACTIVATED', 'vfbp-notifications' ),
		);

		$vfbdb->update_metadata( $form_id, 'mailchimp-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'mailchimp-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'mailchimp-api-data', '' );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * mailchimp_initial_settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function mailchimp_initial_settings( $data, $form_id ) {
		$mailchimp_enable      = isset( $data['mailchimp-enable']      ) ? $data['mailchimp-enable']      : '';
		$mailchimp_api         = isset( $data['mailchimp-api']         ) ? $data['mailchimp-api']         : '';
		$mailchimp_api_data    = isset( $data['mailchimp-api-data']    ) ? $data['mailchimp-api-data']    : '';
		$mailchimp_api_message = isset( $data['mailchimp-api-message'] ) ? $data['mailchimp-api-message'] : '';
		$mailchimp_api_status  = isset( $data['mailchimp-api-status']  ) ? $data['mailchimp-api-status']  : '';

		// Default API message
		if ( empty( $mailchimp_api_message ) )
			$mailchimp_api_message = sprintf( '<span class="status-0">%s</span>', __( 'NOT CONNECTED', 'vfbp-notifications' ) );

		// Check status and display license message
		if ( $mailchimp_api_status )
			$mailchimp_api_message = sprintf( '<span class="status-1">%s</span>', $mailchimp_api_message );
		else
			$mailchimp_api_message = sprintf( '<span class="status-0">%s</span>', $mailchimp_api_message );
	?>
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-enable"><strong><?php _e( 'MailChimp' , 'vfbp-notifications'); ?></strong></label>
			</th>
			<td>
				<fieldset>
					<label>
						<input type="hidden" name="settings[mailchimp-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
						<input type="checkbox" name="settings[mailchimp-enable]" id="mailchimp-enable" value="1"<?php checked( $mailchimp_enable, 1 ); ?> /> <?php _e( "Enable MailChimp signups for this form.", 'vfbp-notifications' ); ?>
					</label>
				</fieldset>
			</td>
		</tr>
	</tbody>
	<tbody class="vfb-mailchimp-settings<?php echo !empty( $mailchimp_enable ) ? ' active' : ''; ?>">
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-api"><?php _e( 'MailChimp API Key' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[mailchimp-api]" id="mailchimp-api" value="<?php esc_html_e( $mailchimp_api ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<?php _e( 'Connection Status' , 'vfbp-notifications'); ?>
			</th>
			<td>
				<p>
					<span id="vfb-verified-mailchimp-text" class="vfb-notifications-api-stati">
						<?php echo $mailchimp_api_message; ?>
					</span>
					<a href="#" id="vfb-mailchimp-verify-api" class="button">
						<?php _e( 'Connect to MailChimp', 'vfbp-notifications' ); ?>
						<span class="spinner"></span>
					</a>

					<?php if ( $mailchimp_api_status ) : ?>
						<a href="#" id="vfb-mailchimp-deactivate-api" class="button">
							<?php _e( 'Disconnect', 'vfbp-notifications' ); ?>
							<span class="spinner"></span>
						</a>
					<?php endif; ?>
				</p>
			</td>
		</tr>

		<?php
			if ( !empty( $mailchimp_api_data ) ) {
				$this->mailchimp_settings( $mailchimp_api_data, $form_id );
			}
		?>
	</tbody>
	<?php
	}

	/**
	 * mailchimp_settings function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param mixed $form_id
	 * @return void
	 */
	public function mailchimp_settings( $response, $form_id ) {
		$chimp = json_decode( $response );

		if ( $chimp->total < 1 ) {
			_e( 'No lists found. Add a list to your MailChimp account and try again.', 'vfbp-notifications' );
			return;
		}

		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$list          = isset( $settings['mailchimp-list']          ) ? $settings['mailchimp-list']          : $chimp->data[0]->id;
		$opt_in        = isset( $settings['mailchimp-opt-in']        ) ? $settings['mailchimp-opt-in']        : 0;
		$merge_email   = isset( $settings['mailchimp-merge-email']   ) ? $settings['mailchimp-merge-email']   : '';
		$merge_fname   = isset( $settings['mailchimp-merge-fname']   ) ? $settings['mailchimp-merge-fname']   : '';
		$merge_lname   = isset( $settings['mailchimp-merge-lname']   ) ? $settings['mailchimp-merge-lname']   : '';
		$merge_company = isset( $settings['mailchimp-merge-company'] ) ? $settings['mailchimp-merge-company'] : '';
		$merge_phone   = isset( $settings['mailchimp-merge-phone']   ) ? $settings['mailchimp-merge-phone']   : '';

		$all_fields   = $vfbdb->get_fields( $form_id );
		$email_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'email' ORDER BY field_order ASC" );
		$phone_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'phone' ORDER BY field_order ASC" );
	?>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-list"><?php _e( 'Select a List' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select name="settings[mailchimp-list]" id="mailchimp-list">
					<?php
						foreach ( $chimp->data as $data ) {
							printf( '<option value="%1$s"%2$s>%3$s</option>', esc_attr( $data->id ), selected( $list, $data->id, 0 ), esc_html( $data->name ) );
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-opt-in"><?php _e( 'Double Opt-In' , 'vfbp-notifications' ); ?></label>
			</th>
			<td>
				<fieldset>
					<label>
						<input type="hidden" name="settings[mailchimp-opt-in]" value="0" /> <!-- This sends an unchecked value to the meta table -->
						<input type="checkbox" name="settings[mailchimp-opt-in]" id="mailchimp-opt-in" value="1"<?php checked( $opt_in, 1 ); ?> /> <?php _e( "Send Opt-In Email.", 'vfbp-notifications' ); ?>
					</label>
				</fieldset>
				<p class="description"><?php _e( "A confirmation email will be sent with a link. If they don't click this link, they won't be added to your list." , 'vfbp-notifications' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-merge-email"><?php _e( 'Email Address' , 'vfbp-notifications' ); ?></label>
			</th>
			<td>
				<select id="mailchimp-merge-email" name="settings[mailchimp-merge-email]">
					<option value=""<?php selected( '', $merge_email ); ?>></option>
					<?php
						if ( is_array( $email_fields ) && !empty( $email_fields ) ) {
							foreach ( $email_fields as $email ) {
								$label    = isset( $email['data']['label'] ) ? $email['data']['label'] : '';
								$field_id = $email['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_email, false ) );
							}
						}
					?>
				</select>
				<p class="description"><?php _e( 'An email address is the only required field.' , 'vfbp-notifications' ); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-merge-fname"><?php _e( 'First Name' , 'vfbp-notifications' ); ?></label>
			</th>
			<td>
				<select id="mailchimp-merge-fname" name="settings[mailchimp-merge-fname]">
					<option value=""<?php selected( '', $merge_fname ); ?>></option>
					<?php
						if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
							foreach ( $all_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_fname, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-merge-lname"><?php _e( 'Last Name' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="mailchimp-merge-lname" name="settings[mailchimp-merge-lname]">
					<option value=""<?php selected( '', $merge_lname ); ?>></option>
					<?php
						if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
							foreach ( $all_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_lname, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-merge-company"><?php _e( 'Company' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="mailchimp-merge-company" name="settings[mailchimp-merge-company]">
					<option value=""<?php selected( '', $merge_company ); ?>></option>
					<?php
						if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
							foreach ( $all_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_company, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mailchimp-merge-phone"><?php _e( 'Phone' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="mailchimp-merge-phone" name="settings[mailchimp-merge-phone]">
					<option value=""<?php selected( '', $merge_phone ); ?>></option>
					<?php
						if ( is_array( $phone_fields ) && !empty( $phone_fields ) ) {
							foreach ( $phone_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_phone, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
	<?php
	}

	/**
	 * connect_campaign_montior function.
	 *
	 * @access public
	 * @return void
	 */
	public function connect_campaign_monitor() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-connect-campaign-monitor' !== $_GET['action'] )
			return;

		$api_key = isset( $_GET['api']  ) ? esc_html( $_GET['api'] ) : '';
		$form_id = isset( $_GET['form'] ) ? absint( $_GET['form']  ) : '';

		$vfbdb = new VFB_Pro_Data();

		$api_endpoint = 'https://api.createsend.com/api/v3/clients.json';

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_get(
			"$api_endpoint",
			array(
				'sslverify' => false,
				'headers'   => $headers,
			)
		);

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {

			$response = array(
				'status'  => 0,
				'message' => __( 'No Campaign Monitor account found. Check the API key correctly.', 'vfbp-notifications' ),
			);

			$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-status', $response['status'] );
			$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-message', $response['message'] );

			echo json_encode( $response );

			die(0);
		}

		$response = array(
			'status'  => 1,
			'message' => __( 'VERIFIED', 'vfbp-notifications' ),
		);

		// Retrieve and set response
		$api_response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-data', $api_response );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * disconnect_campaign_monitor function.
	 *
	 * @access public
	 * @return void
	 */
	public function disconnect_campaign_monitor() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-disconnect-campaign-monitor' !== $_GET['action'] )
			return;

		$form_id = isset( $_GET['form'] ) ? absint( $_GET['form']  ) : '';

		$vfbdb = new VFB_Pro_Data();

		$response = array(
			'status'  => 0,
			'message' => __( 'DEACTIVATED', 'vfbp-notifications' ),
		);

		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-data', '' );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * campaign_monitor_select_client function.
	 *
	 * @access public
	 * @return void
	 */
	public function campaign_monitor_select_client() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-campaign-monitor-select-client' !== $_GET['action'] )
			return;

		$api_key   = isset( $_GET['api']    ) ? esc_html( $_GET['api']  ) : '';
		$form_id   = isset( $_GET['form']   ) ? absint( $_GET['form']   ) : '';
		$client_id = isset( $_GET['client'] ) ? esc_html( $_GET['client'] ) : '';

		$vfbdb = new VFB_Pro_Data();

		$api_endpoint = "https://api.createsend.com/api/v3.1/clients/{$client_id}/lists.json";

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_get(
			"$api_endpoint",
			array(
				'sslverify' => false,
				'headers'   => $headers,
			)
		);

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {
			_e( 'No lists found. Please try again.', 'vfbp-notifications' );
			die(0);
		}

		// Retrieve and set response
		$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		$vfbdb->update_metadata( $form_id, 'campaign-monitor-api-lists', $response );

		$this->campaign_monitor_list_settings( $response, $form_id );

		die(1);
	}

	/**
	 * campaign_monitor_initial_settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @param mixed $form_id
	 * @return void
	 */
	public function campaign_monitor_initial_settings( $data, $form_id ) {
		$enable      = isset( $data['campaign-monitor-enable']      ) ? $data['campaign-monitor-enable']      : '';
		$api         = isset( $data['campaign-monitor-api']         ) ? $data['campaign-monitor-api']         : '';
		$api_data    = isset( $data['campaign-monitor-api-data']    ) ? $data['campaign-monitor-api-data']    : '';
		$api_lists   = isset( $data['campaign-monitor-api-lists']   ) ? $data['campaign-monitor-api-lists']   : '';
		$api_message = isset( $data['campaign-monitor-api-message'] ) ? $data['campaign-monitor-api-message'] : '';
		$api_status  = isset( $data['campaign-monitor-api-status']  ) ? $data['campaign-monitor-api-status']  : '';

		// Default API message
		if ( empty( $api_message ) )
			$api_message = sprintf( '<span class="status-0">%s</span>', __( 'NOT CONNECTED', 'vfbp-notifications' ) );

		// Check status and display license message
		if ( $api_status )
			$api_message = sprintf( '<span class="status-1">%s</span>', $api_message );
		else
			$api_message = sprintf( '<span class="status-0">%s</span>', $api_message );
	?>
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="campaign-monitor-enable"><strong><?php _e( 'Campaign Monitor' , 'vfbp-notifications'); ?></strong></label>
			</th>
			<td>
				<fieldset>
					<label>
						<input type="hidden" name="settings[campaign-monitor-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
						<input type="checkbox" name="settings[campaign-monitor-enable]" id="campaign-monitor-enable" value="1"<?php checked( $enable, 1 ); ?> /> <?php _e( "Enable Campaign Monitor signups for this form.", 'vfbp-notifications' ); ?>
					</label>
				</fieldset>
			</td>
		</tr>
	</tbody>
	<tbody class="vfb-campaign-monitor-settings<?php echo !empty( $enable ) ? ' active' : ''; ?>">
		<tr valign="top">
			<th scope="row">
				<label for="campaign-monitor-api"><?php _e( 'Campaign Monitor API Key' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[campaign-monitor-api]" id="campaign-monitor-api" value="<?php esc_html_e( $api ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<?php _e( 'Connection Status' , 'vfbp-notifications'); ?>
			</th>
			<td>
				<p>
					<span id="vfb-verified-campaign-monitor-text" class="vfb-notifications-api-stati">
						<?php echo $api_message; ?>
					</span>
					<a href="#" id="vfb-campaign-monitor-verify-api" class="button">
						<?php _e( 'Connect to Campaign Monitor', 'vfbp-notifications' ); ?>
						<span class="spinner"></span>
					</a>

					<?php if ( $api_status ) : ?>
						<a href="#" id="vfb-campaign-monitor-deactivate-api" class="button">
							<?php _e( 'Disconnect', 'vfbp-notifications' ); ?>
							<span class="spinner"></span>
						</a>
					<?php endif; ?>
				</p>
			</td>
		</tr>

		<?php
			if ( !empty( $api_data ) ) {
				$this->campaign_monitor_client_settings( $api_data, $form_id );

				if ( !empty( $api_lists ) ) {
					$this->campaign_monitor_list_settings( $api_lists, $form_id );
				}
			}
		?>
	</tbody>
	<?php
	}

	/**
	 * campaign_monitor_client_settings function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param mixed $form_id
	 * @return void
	 */
	public function campaign_monitor_client_settings( $response, $form_id ) {
		$campaign = json_decode( $response );

		if ( count( $campaign ) < 1 ) {
			_e( 'No clients found. Add a client to your Campaign Monitor account and try again.', 'vfbp-notifications' );
			return;
		}

		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$client = isset( $settings['campaign-monitor-client-id'] ) ? $settings['campaign-monitor-client-id'] : $campaign[0]->ClientID;
	?>
	<tr valign="top" id="vfb-campaign-monitor-client-response">
		<th scope="row">
			<label for="campaign-monitor-client-id"><?php _e( 'Select a Client' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select name="settings[campaign-monitor-client-id]" id="campaign-monitor-client-id">
				<?php
					foreach ( $campaign as $data ) {
						printf( '<option value="%1$s"%2$s>%3$s</option>', esc_attr( $data->ClientID ), selected( $client, $data->ClientID, 0 ), esc_html( $data->Name ) );
					}
				?>
			</select>
			<a href="#" id="vfb-campaign-monitor-select-client" class="button">
				<?php _e( 'Select this Client', 'vfbp-notifications' ); ?>
				<span class="spinner"></span>
			</a>
		</td>
	</tr>
	<?php
	}

	/**
	 * campaign_monitor_list_settings function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param mixed $form_id
	 * @return void
	 */
	public function campaign_monitor_list_settings( $response, $form_id ) {
		$campaign = json_decode( $response );

		if ( count( $campaign ) < 1 ) {
			_e( 'No lists found. Add a list to the selected client in Campaign Monitor and try again.', 'vfbp-notifications' );
			return;
		}

		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$list        = isset( $settings['campaign-monitor-list-id']     ) ? $settings['campaign-monitor-list-id']     : $campaign[0]->ListID;
		$merge_email = isset( $settings['campaign-monitor-merge-email'] ) ? $settings['campaign-monitor-merge-email'] : '';
		$merge_fname = isset( $settings['campaign-monitor-merge-fname'] ) ? $settings['campaign-monitor-merge-fname'] : '';
		$merge_lname = isset( $settings['campaign-monitor-merge-lname'] ) ? $settings['campaign-monitor-merge-lname'] : '';

		$all_fields   = $vfbdb->get_fields( $form_id );
		$email_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'email' ORDER BY field_order ASC" );
	?>
	<tr valign="top">
		<th scope="row">
			<label for="campaign-monitor-list-id"><?php _e( 'Select a List' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select name="settings[campaign-monitor-list-id]" id="campaign-monitor-list-id">
				<?php
					foreach ( $campaign as $data ) {
						printf( '<option value="%1$s"%2$s>%3$s</option>', esc_attr( $data->ListID ), selected( $list, $data->ListID, 0 ), esc_html( $data->Name ) );
					}
				?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="campaign-monitor-merge-email"><?php _e( 'Email Address' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="campaign-monitor-merge-email" name="settings[campaign-monitor-merge-email]">
				<option value=""<?php selected( '', $merge_email ); ?>></option>
				<?php
					if ( is_array( $email_fields ) && !empty( $email_fields ) ) {
						foreach ( $email_fields as $email ) {
							$label    = isset( $email['data']['label'] ) ? $email['data']['label'] : '';
							$field_id = $email['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_email, false ) );
						}
					}
				?>
			</select>
			<p class="description"><?php _e( 'An email address is the only required field.' , 'vfbp-notifications'); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="campaign-monitor-merge-fname"><?php _e( 'First Name' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="campaign-monitor-merge-fname" name="settings[campaign-monitor-merge-fname]">
				<option value=""<?php selected( '', $merge_fname ); ?>></option>
				<?php
					if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
						foreach ( $all_fields as $field ) {
							$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
							$field_id = $field['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_fname, false ) );
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="campaign-monitor-merge-lname"><?php _e( ' Last Name' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="campaign-monitor-merge-lname" name="settings[campaign-monitor-merge-lname]">
				<option value=""<?php selected( '', $merge_lname ); ?>></option>
				<?php
					if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
						foreach ( $all_fields as $field ) {
							$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
							$field_id = $field['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_lname, false ) );
						}
					}
				?>
			</select>
		</td>
	</tr>
	<?php
	}

	/**
	 * connect_highrise function.
	 *
	 * @access public
	 * @return void
	 */
	public function connect_highrise() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-connect-highrise' !== $_GET['action'] )
			return;

		$api_key   = isset( $_GET['api']    ) ? esc_html( $_GET['api']    ) : '';
		$subdomain = isset( $_GET['domain'] ) ? esc_html( $_GET['domain'] ) : '';
		$form_id   = isset( $_GET['form']   ) ? absint( $_GET['form']     ) : '';

		$vfbdb = new VFB_Pro_Data();

		$args = array(
			'apikey'	=> $api_key,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$request = wp_remote_get(
			"https://{$subdomain}.highrisehq.com/account.xml",
			array(
				'sslverify' => false,
				'headers'   => $headers,
			)
		);

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {

			$response = array(
				'status'  => 0,
				'message' => __( 'No Highrise account found. Check the subdomain and API key correctly.', 'vfbp-notifications' ),
			);

			$vfbdb->update_metadata( $form_id, 'highrise-api-status', $response['status'] );
			$vfbdb->update_metadata( $form_id, 'highrise-api-message', $response['message'] );

			echo json_encode( $response );

			die(0);
		}

		$response = array(
			'status'  => 1,
			'message' => __( 'VERIFIED', 'vfbp-notifications' ),
		);

		// Retrieve and set response
		$api_response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		$vfbdb->update_metadata( $form_id, 'highrise-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'highrise-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'highrise-api-data', $api_response );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * disconnect_highrise function.
	 *
	 * @access public
	 * @return void
	 */
	public function disconnect_highrise() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-disconnect-highrise' !== $_GET['action'] )
			return;

		$form_id = isset( $_GET['form'] ) ? absint( $_GET['form']  ) : '';

		$vfbdb = new VFB_Pro_Data();

		$response = array(
			'status'  => 0,
			'message' => __( 'DEACTIVATED', 'vfbp-notifications' ),
		);

		$vfbdb->update_metadata( $form_id, 'highrise-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'highrise-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'highrise-api-data', '' );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * highrise_initial_settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @return void
	 */
	public function highrise_initial_settings( $data, $form_id ) {
		$enable      = isset( $data['highrise-enable']      ) ? $data['highrise-enable']      : '';
		$api         = isset( $data['highrise-api']         ) ? $data['highrise-api']         : '';
		$api_data    = isset( $data['highrise-api-data']    ) ? $data['highrise-api-data']    : '';
		$api_message = isset( $data['highrise-api-message'] ) ? $data['highrise-api-message'] : '';
		$api_status  = isset( $data['highrise-api-status']  ) ? $data['highrise-api-status']  : '';
		$subdomain   = isset( $data['highrise-subdomain']   ) ? $data['highrise-subdomain']   : '';

		// Default API message
		if ( empty( $api_message ) )
			$api_message = sprintf( '<span class="status-0">%s</span>', __( 'NOT CONNECTED', 'vfbp-notifications' ) );

		// Check status and display license message
		if ( $api_status )
			$api_message = sprintf( '<span class="status-1">%s</span>', $api_message );
		else
			$api_message = sprintf( '<span class="status-0">%s</span>', $api_message );
	?>
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-enable"><strong><?php _e( 'Highrise' , 'vfbp-notifications'); ?></strong></label>
			</th>
			<td>
				<fieldset>
					<label>
						<input type="hidden" name="settings[highrise-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
						<input type="checkbox" name="settings[highrise-enable]" id="highrise-enable" value="1"<?php checked( $enable, 1 ); ?> /> <?php _e( "Enable Highrise signups for this form.", 'vfbp-notifications' ); ?>
					</label>
				</fieldset>
			</td>
		</tr>
	</tbody>
	<tbody class="vfb-highrise-settings<?php echo !empty( $enable ) ? ' active' : ''; ?>">
		<tr valign="top">
			<th scope="row">
				<label for="highrise-api"><?php _e( 'Highrise API Key' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[highrise-api]" id="highrise-api" value="<?php esc_html_e( $api ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-subdomain"><?php _e( 'Highrise Subdomain' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[highrise-subdomain]" id="highrise-subdomain" value="<?php esc_html_e( $subdomain ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<?php _e( 'Connection Status' , 'vfbp-notifications'); ?>
			</th>
			<td>
				<p>
					<span id="vfb-verified-highrise-text" class="vfb-notifications-api-stati">
						<?php echo $api_message; ?>
					</span>
					<a href="#" id="vfb-highrise-verify-api" class="button">
						<?php _e( 'Connect to Highrise', 'vfbp-notifications' ); ?>
						<span class="spinner"></span>
					</a>

					<?php if ( $api_status ) : ?>
						<a href="#" id="vfb-highrise-deactivate-api" class="button">
							<?php _e( 'Disconnect', 'vfbp-notifications' ); ?>
							<span class="spinner"></span>
						</a>
					<?php endif; ?>
				</p>
			</td>
		</tr>

		<?php
			if ( !empty( $api_data ) ) {
				$this->highrise_settings( $api_data, $form_id );
			}
		?>
	</tbody>
	<?php
	}

	/**
	 * highrise_settings function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param mixed $form_id
	 * @return void
	 */
	public function highrise_settings( $response, $form_id ) {
		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$merge_email   = isset( $settings['highrise-merge-email']   ) ? $settings['highrise-merge-email']   : '';
		$merge_fname   = isset( $settings['highrise-merge-fname']   ) ? $settings['highrise-merge-fname']   : '';
		$merge_lname   = isset( $settings['highrise-merge-lname']   ) ? $settings['highrise-merge-lname']   : '';
		$merge_company = isset( $settings['highrise-merge-company'] ) ? $settings['highrise-merge-company'] : '';
		$merge_phone   = isset( $settings['highrise-merge-phone']   ) ? $settings['highrise-merge-phone']   : '';
		$note          = isset( $settings['highrise-note']          ) ? $settings['highrise-note']          : '';

		$all_fields   = $vfbdb->get_fields( $form_id );
		$email_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'email' ORDER BY field_order ASC" );
		$phone_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'phone' ORDER BY field_order ASC" );
	?>
		<tr valign="top">
			<th scope="row">
				<label for="merge-email"><?php _e( 'Email Address' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="merge-email" name="settings[highrise-merge-email]">
					<option value=""<?php selected( '', $merge_email ); ?>></option>
					<?php
						if ( is_array( $email_fields ) && !empty( $email_fields ) ) {
							foreach ( $email_fields as $email ) {
								$label    = isset( $email['data']['label'] ) ? $email['data']['label'] : '';
								$field_id = $email['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_email, false ) );
							}
						}
					?>
				</select>
				<p class="description"><?php _e( 'An email address is the only required field.' , 'vfbp-notifications'); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-merge-fname"><?php _e( 'First Name' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="highrise-merge-fname" name="settings[highrise-merge-fname]">
					<option value=""<?php selected( '', $merge_fname ); ?>></option>
					<?php
						if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
							foreach ( $all_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_fname, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-merge-lname"><?php _e( 'Last Name' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="highrise-merge-lname" name="settings[highrise-merge-lname]">
					<option value=""<?php selected( '', $merge_lname ); ?>></option>
					<?php
						if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
							foreach ( $all_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_lname, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-merge-company"><?php _e( 'Company' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="highrise-merge-company" name="settings[highrise-merge-company]">
					<option value=""<?php selected( '', $merge_company ); ?>></option>
					<?php
						if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
							foreach ( $all_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_company, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-merge-phone"><?php _e( 'Phone' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<select id="highrise-merge-phone" name="settings[highrise-merge-phone]">
					<option value=""<?php selected( '', $merge_phone ); ?>></option>
					<?php
						if ( is_array( $phone_fields ) && !empty( $phone_fields ) ) {
							foreach ( $phone_fields as $field ) {
								$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
								$field_id = $field['id'];

								printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_phone, false ) );
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="highrise-note"><?php _e( 'Attach a Note' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<textarea name="settings[highrise-note]" id="highrise-note" class="large-text" rows="5"><?php echo $note; ?></textarea>
			</td>
		</tr>
	<?php
	}

	/**
	 * connect_freshbooks function.
	 *
	 * @access public
	 * @return void
	 */
	public function connect_freshbooks() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-connect-freshbooks' !== $_GET['action'] )
			return;

		$api_key   = isset( $_GET['api']    ) ? esc_html( $_GET['api']    ) : '';
		$subdomain = isset( $_GET['domain'] ) ? esc_html( $_GET['domain'] ) : '';
		$form_id   = isset( $_GET['form']   ) ? absint( $_GET['form']     ) : '';

		$vfbdb = new VFB_Pro_Data();

		$args = array(
			'apikey'	=> $api_key,
		);

		$headers = array(
			'Authorization' => 'Basic ' . base64_encode( $api_key . ':nopass' )
		);

		$body = '<?xml version="1.0" encoding="utf-8"?>
		<request method="system.current">
		</request>';

		$request = wp_remote_post(
			"https://{$subdomain}.freshbooks.com/api/2.1/xml-in",
			array(
				'sslverify' => false,
				'body'      => $body,
				'headers'   => $headers,
			)
		);

		// If request fails, stop
		if ( is_wp_error( $request ) ||	wp_remote_retrieve_response_code( $request ) != 200	) {

			$response = array(
				'status'  => 0,
				'message' => __( 'No Freshbooks account found. Check the subdomain and API key correctly.', 'vfbp-notifications' ),
			);

			$vfbdb->update_metadata( $form_id, 'freshbooks-api-status', $response['status'] );
			$vfbdb->update_metadata( $form_id, 'freshbooks-api-message', $response['message'] );

			echo json_encode( $response );

			die(0);
		}

		$response = array(
			'status'  => 1,
			'message' => __( 'VERIFIED', 'vfbp-notifications' ),
		);

		// Retrieve and set response
		$api_response = maybe_unserialize( wp_remote_retrieve_body( $request ) );

		$vfbdb->update_metadata( $form_id, 'freshbooks-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'freshbooks-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'freshbooks-api-data', $api_response );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * disconnect_freshbooks function.
	 *
	 * @access public
	 * @return void
	 */
	public function disconnect_freshbooks() {
		// Check AJAX nonce set via wp_localize_script
		check_ajax_referer( 'vfbp_ajax', 'vfbp_ajax_nonce' );

		if ( isset( $_GET['action'] ) && 'vfbp-disconnect-freshbooks' !== $_GET['action'] )
			return;

		$form_id = isset( $_GET['form'] ) ? absint( $_GET['form']  ) : '';

		$vfbdb = new VFB_Pro_Data();

		$response = array(
			'status'  => 0,
			'message' => __( 'DEACTIVATED', 'vfbp-notifications' ),
		);

		$vfbdb->update_metadata( $form_id, 'freshbooks-api-status', $response['status'] );
		$vfbdb->update_metadata( $form_id, 'freshbooks-api-message', $response['message'] );
		$vfbdb->update_metadata( $form_id, 'freshbooks-api-data', '' );

		echo json_encode( $response );

		die(1);
	}

	/**
	 * freshbooks_initial_settings function.
	 *
	 * @access public
	 * @param mixed $data
	 * @param mixed $form_id
	 * @return void
	 */
	public function freshbooks_initial_settings( $data, $form_id ) {
		$enable      = isset( $data['freshbooks-enable']      ) ? $data['freshbooks-enable']      : '';
		$api         = isset( $data['freshbooks-api']         ) ? $data['freshbooks-api']         : '';
		$api_data    = isset( $data['freshbooks-api-data']    ) ? $data['freshbooks-api-data']    : '';
		$api_message = isset( $data['freshbooks-api-message'] ) ? $data['freshbooks-api-message'] : '';
		$api_status  = isset( $data['freshbooks-api-status']  ) ? $data['freshbooks-api-status']  : '';
		$subdomain   = isset( $data['freshbooks-subdomain']   ) ? $data['freshbooks-subdomain']   : '';

		// Default API message
		if ( empty( $api_message ) )
			$api_message = sprintf( '<span class="status-0">%s</span>', __( 'NOT CONNECTED', 'vfbp-notifications' ) );

		// Check status and display license message
		if ( $api_status )
			$api_message = sprintf( '<span class="status-1">%s</span>', $api_message );
		else
			$api_message = sprintf( '<span class="status-0">%s</span>', $api_message );
	?>
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="freshbooks-enable"><strong><?php _e( 'Freshbooks' , 'vfbp-notifications'); ?></strong></label>
			</th>
			<td>
				<fieldset>
					<label>
						<input type="hidden" name="settings[freshbooks-enable]" value="0" /> <!-- This sends an unchecked value to the meta table -->
						<input type="checkbox" name="settings[freshbooks-enable]" id="freshbooks-enable" value="1"<?php checked( $enable, 1 ); ?> /> <?php _e( "Enable Freshbooks signups for this form.", 'vfbp-notifications' ); ?>
					</label>
				</fieldset>
			</td>
		</tr>
	</tbody>
	<tbody class="vfb-freshbooks-settings<?php echo !empty( $enable ) ? ' active' : ''; ?>">
		<tr valign="top">
			<th scope="row">
				<label for="freshbooks-api"><?php _e( 'Freshbooks API Key' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[freshbooks-api]" id="freshbooks-api" value="<?php esc_html_e( $api ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="freshbooks-subdomain"><?php _e( 'Freshbooks Subdomain' , 'vfbp-notifications'); ?></label>
			</th>
			<td>
				<input type="text" name="settings[freshbooks-subdomain]" id="freshbooks-subdomain" value="<?php esc_html_e( $subdomain ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<?php _e( 'Connection Status' , 'vfbp-notifications'); ?>
			</th>
			<td>
				<p>
					<span id="vfb-verified-freshbooks-text" class="vfb-notifications-api-stati">
						<?php echo $api_message; ?>
					</span>
					<a href="#" id="vfb-freshbooks-verify-api" class="button">
						<?php _e( 'Connect to Freshbooks', 'vfbp-notifications' ); ?>
						<span class="spinner"></span>
					</a>

					<?php if ( $api_status ) : ?>
						<a href="#" id="vfb-freshbooks-deactivate-api" class="button">
							<?php _e( 'Disconnect', 'vfbp-notifications' ); ?>
							<span class="spinner"></span>
						</a>
					<?php endif; ?>
				</p>
			</td>
		</tr>

		<?php
			if ( !empty( $api_data ) ) {
				$this->freshbooks_settings( $api_data, $form_id );
			}
		?>
	</tbody>
	<?php
	}

	/**
	 * freshbooks_settings function.
	 *
	 * @access public
	 * @param mixed $response
	 * @param mixed $form_id
	 * @return void
	 */
	public function freshbooks_settings( $response, $form_id ) {
		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$merge_email   = isset( $settings['freshbooks-merge-email']   ) ? $settings['freshbooks-merge-email']   : '';
		$merge_fname   = isset( $settings['freshbooks-merge-fname']   ) ? $settings['freshbooks-merge-fname']   : '';
		$merge_lname   = isset( $settings['freshbooks-merge-lname']   ) ? $settings['freshbooks-merge-lname']   : '';
		$merge_company = isset( $settings['freshbooks-merge-company'] ) ? $settings['freshbooks-merge-company'] : '';
		$merge_phone   = isset( $settings['freshbooks-merge-phone']   ) ? $settings['freshbooks-merge-phone']   : '';
		$note          = isset( $settings['freshbooks-note']          ) ? $settings['freshbooks-note']          : '';

		$all_fields   = $vfbdb->get_fields( $form_id );
		$email_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'email' ORDER BY field_order ASC" );
		$phone_fields = $vfbdb->get_fields( $form_id, "AND field_type = 'phone' ORDER BY field_order ASC" );
	?>
	<tr valign="top">
		<th scope="row">
			<label for="freshbooks-merge-email"><?php _e( 'Email Address' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="freshbooks-merge-email" name="settings[freshbooks-merge-email]">
				<option value=""<?php selected( '', $merge_email ); ?>></option>
				<?php
					if ( is_array( $email_fields ) && !empty( $email_fields ) ) {
						foreach ( $email_fields as $email ) {
							$label    = isset( $email['data']['label'] ) ? $email['data']['label'] : '';
							$field_id = $email['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_email, false ) );
						}
					}
				?>
			</select>
			<p class="description"><?php _e( 'An email address is the only required field.' , 'vfbp-notifications'); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="freshbooks-merge-fname"><?php _e( 'First Name' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="freshbooks-merge-fname" name="settings[freshbooks-merge-fname]">
				<option value=""<?php selected( '', $merge_fname ); ?>></option>
				<?php
					if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
						foreach ( $all_fields as $field ) {
							$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
							$field_id = $field['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_fname, false ) );
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="freshbooks-merge-lname"><?php _e( 'Last Name' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="freshbooks-merge-lname" name="settings[freshbooks-merge-lname]">
				<option value=""<?php selected( '', $merge_lname ); ?>></option>
				<?php
					if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
						foreach ( $all_fields as $field ) {
							$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
							$field_id = $field['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_lname, false ) );
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="freshbooks-merge-company"><?php _e( 'Company' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="freshbooks-merge-company" name="settings[freshbooks-merge-company]">
				<option value=""<?php selected( '', $merge_company ); ?>></option>
				<?php
					if ( is_array( $all_fields ) && !empty( $all_fields ) ) {
						foreach ( $all_fields as $field ) {
							$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
							$field_id = $field['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_company, false ) );
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="freshbooks-merge-phone"><?php _e( 'Phone' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<select id="freshbooks-merge-phone" name="settings[freshbooks-merge-phone]">
				<option value=""<?php selected( '', $merge_phone ); ?>></option>
				<?php
					if ( is_array( $phone_fields ) && !empty( $phone_fields ) ) {
						foreach ( $phone_fields as $field ) {
							$label    = isset( $field['data']['label'] ) ? $field['data']['label'] : '';
							$field_id = $field['id'];

							printf( '<option value="%1$d"%3$s>%1$d - %2$s</option>', $field_id, $label, selected( $field_id, $merge_phone, false ) );
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<label for="freshbooks-note"><?php _e( 'Attach a Note' , 'vfbp-notifications'); ?></label>
		</th>
		<td>
			<textarea name="settings[freshbooks-note]" id="freshbooks-note" class="large-text" rows="5"><?php echo $note; ?></textarea>
		</td>
	</tr>
	<?php
	}
}