<?php
/**
 * The main function for the add-on
 *
 * @since      2.0
 */
class VFB_Pro_Addon_Create_User_Main {
	/**
	 * username
	 *
	 * @var mixed
	 * @access protected
	 */
	protected $username;

	/**
	 * password
	 *
	 * @var mixed
	 * @access protected
	 */
	protected $password;

	/**
	 * email
	 *
	 * @var mixed
	 * @access protected
	 */
	protected $email;

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		add_action( 'vfbp_after_save_entry', array( $this, 'init' ), 10, 2 );
	}

	/**
	 * init function.
	 *
	 * @access public
	 * @param mixed $entry_id
	 * @param mixed $form_id
	 * @return void
	 */
	public function init( $entry_id, $form_id ) {

		$vfbdb    = new VFB_Pro_Data();
		$settings = $vfbdb->get_addon_settings( $form_id );

		$user_enable = isset( $settings['user-enable']       ) ? $settings['user-enable']       : '';
		$user_notify = isset( $settings['user-notify-email'] ) ? $settings['user-notify-email'] : '';
		$user_role   = isset( $settings['user-role']         ) ? $settings['user-role']         : 'subscriber';
		$user_email  = isset( $settings['user-email']        ) ? $settings['user-email']        : '';
		$username    = isset( $settings['user-username']     ) ? $settings['user-username']     : '';
		$password    = isset( $settings['user-password']     ) ? $settings['user-password']     : '';

		// Exit if not enabled for this form
		if ( empty( $user_enable ) )
			return;

		// Loop through entry data and set vars
		$data = array();
		$meta = get_post_meta( $entry_id );
		foreach ( $meta as $key => $value ) {
			$field_id = str_replace( '_vfb_field-', '', $key );

			// Set user vars
			switch ( $field_id ) {
				case $user_email :
					$data['user_email'] = $value[0];
					break;

				case $username :
					$data['user_login'] = $value[0];
					break;

				case $password :
					$data['user_pass'] = $value[0];
					break;
			}
		}

		// If any field is empty, output an error
		if ( empty( $data['user_login'] ) || empty( $data['user_pass'] ) || empty( $data['user_email'] ) )
			wp_die( '<h1>Missing User Information</h1><br>' . __( 'A username, password, and email are required to create a user', 'vfbp-create-user' ), '', array( 'back_link' => true ) );

		$user_id = username_exists( $data['user_login'] );

		if ( $user_id )
			wp_die( __( 'Username already exists. Please try again.', 'vfbp-create-user' ), '', array( 'back_link' => true ) );

		if ( email_exists( $data['user_email'] ) )
			wp_die( __( 'Email is registered to another user. Please try again.', 'vfbp-create-user' ), '', array( 'back_link' => true ) );

		// Set User Role
		$data['role'] = $user_role;

		// Create the user
		$new_id = wp_insert_user( $data );

		// If successful, send notifcation email
		if ( $new_id ) {

			if ( !empty( $user_notify ) )
				wp_new_user_notification( $new_id, $data['user_pass'] );
		}
	}
}